<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' , 'WatchesController@index');

Route::get('/watch_detail/{id}', 'WatchesController@watch_detail');

Route::get('/about', 'WatchesController@about');

Route::get('/hers', 'WatchesController@hers');

Route::get('/his', 'WatchesController@his');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/search', 'WatchesController@search');

Route::get('/test', 'WatchesController@test');


Route::get('/profile', 'ProfileController@show');

Route::get('/edit_profile', 'ProfileController@edit_profile');

Route::patch('/profile', 'ProfileController@update_profile');

Route::get('/change_password', 'ProfileController@change_password');

Route::patch('/change_password', 'ProfileController@update_password');


Route::get('/cart', 'PagesController@cart');

Route::post('/add_watch', 'PagesController@addToCart');

Route::post('/update', 'PagesController@update');

Route::get('/checkout', 'PagesController@checkout');

Route::post('/confirm', 'PagesController@confirm');


// Admin Watches
Route::get('admin/watches', 'Admin\WatchesController@index');

Route::get('admin/watches/create', 'Admin\WatchesController@create');

Route::post('/admin/watches', 'Admin\WatchesController@store');

Route::get('admin/watches/{watch}/edit', 'Admin\WatchesController@edit');

Route::patch('/admin/watches', 'Admin\WatchesController@update');


Route::get('admin/watches/{id}', 'Admin\WatchesController@destroy');


// Categories
Route::get('admin/category', 'Admin\CategoriesController@index');

Route::get('admin/category/create', 'Admin\CategoriesController@create');

Route::post('/admin/category', 'Admin\CategoriesController@store');

Route::get('admin/category/{category}/edit', 'Admin\CategoriesController@edit');

Route::patch('/admin/category', 'Admin\CategoriesController@update');

Route::get('/admin/category/{id}', 'Admin\CategoriesController@destroy');


// Tags
Route::get('admin/tag', 'Admin\TagsController@index');

Route::get('admin/tag/create', 'Admin\TagsController@create');

Route::post('/admin/tag', 'Admin\TagsController@store');

Route::get('admin/tag/{tag}/edit', 'Admin\TagsController@edit');

Route::patch('/admin/tag', 'Admin\TagsController@update');

Route::get('/admin/tag/{id}', 'Admin\TagsController@destroy');


// Users
Route::get('admin/admin_users/index', 'Admin\UsersController@index');

Route::get('admin/admin_users/create', 'Admin\UsersController@create');

Route::post('admin/admin_users/index', 'Admin\UsersController@store');

Route::patch('admin/admin_users/index', 'Admin\UsersController@update');

Route::get('admin/admin_users/{{$users->id}}', 'Admin\UsersController@show');

Route::get('admin/admin_users/{users}/edit', 'Admin\UsersController@edit');

Route::delete('admin/admin_users/{id}','Admin\UsersController@destroy');

// Orders
Route::get('admin/orders', 'Admin\OrdersController@index');

Route::get('admin/orders/create', 'Admin\OrdersController@create');

Route::post('/admin/orders', 'Admin\OrdersController@store');

Route::get('admin/order/{order}/edit', 'Admin\OrdersController@edit');

Route::patch('admin/orders', 'Admin\OrdersController@update');

Route::get('admin/orders/delete/{id}', 'Admin\OrdersController@destroy');

Route::get('admin', 'Admin\AdminController@dashboard');

// Route::get('admin', 'admin\AdminsController@create');















