@extends('layouts.app')

@section('content')

<div id="thankyou">

    <div id="thankyou_title" class="row">

        <div class="col-sm-12 shopp_line">
            <h3>Thank you for your order!</h3>
            <h3>See Your Order Details</h3>
            <hr>
        </div><!-- /.col-sm-12 shopp_line -->

    </div><!-- /.row -->

    <div class="row thankyou_info">

        <h3>Shipping Information:</h3>

        <div class="col-sm-2"></div>
        <div class="col-sm-4 pay_left">Shipping Address: </div>
        <div class="col-sm-4 pay_right">{{$order->shipping_street}}</div>
        <div class="col-sm-2"></div>

    </div><!-- .row -->

    <div class="row thankyou_info">

        <div class="col-sm-2"></div>
        <div class="col-sm-4 pay_left">Postal Code: </div>
        <div class="col-sm-4 pay_right">{{$order->shipping_postal}}</div>
        <div class="col-sm-2"></div>

    </div><!-- .row -->

    <div class="row thankyou_info">

        <div class="col-sm-2"></div>
        <div class="col-sm-4 pay_left">City: </div>
        <div class="col-sm-4 pay_right">{{$order->shipping_city}}</div>
        <div class="col-sm-2"></div>

    </div><!-- .row -->

    <div class="row thankyou_info">

        <div class="col-sm-2"></div>
        <div class="col-sm-4 pay_left">Province: </div>
        <div class="col-sm-4 pay_right">{{$order->shipping_province}}</div>
        <div class="col-sm-2"></div>

    </div><!-- .row -->

    <div class="row thankyou_info">

        <div class="col-sm-2"></div>
        <div class="col-sm-4 pay_left">Country: </div>
        <div class="col-sm-4 pay_right">{{$order->shipping_country}}</div>
        <div class="col-sm-2"></div>
        <hr>

    </div><!-- .row -->

    <div class="row thankyou_info">

        <h3>Order Information:</h3>

        <div class="col-sm-2"></div>
        <div class="col-sm-4 pay_left">Order ID: </div>
        <div class="col-sm-4 pay_right">{{$order->id}}</div>
        <div class="col-sm-2"></div>

    </div><!-- .row -->

    <div class="row thankyou_info">

        <div class="col-sm-2"></div>
        <div class="col-sm-4 pay_left">Date: </div>
        <div class="col-sm-4 pay_right">{{$order->created_at->formatLocalized('%A %d %B %Y')}}</div>
        <div class="col-sm-2"></div>

    </div><!-- .row -->

    <div class="row thankyou_info">

        <div class="col-sm-2"></div>
        <div class="col-sm-4 pay_left">Name: </div>
        <div class="col-sm-4 pay_right">{{$order->name}}</div>
        <div class="col-sm-2"></div>

    </div><!-- .row -->

    <div class="row thankyou_info">

        <div class="col-sm-2"></div>
        <div class="col-sm-4 pay_left">Email: </div>
        <div class="col-sm-4 pay_right">{{$order->email}}</div>
        <div class="col-sm-2"></div>
        <hr>

    </div><!-- .row -->

    <?php foreach($items->all() as $item) :?>

        <div class="row thankyou_info">

            <h3>Selected Items:</h3>

            <div class="col-sm-2"></div>
            <div class="col-sm-4 pay_left">Item: </div>
            <div class="col-sm-4 pay_right">{{$item->name}}</div>
            <div class="col-sm-2"></div>

        </div><!-- .row -->

        <div class="row thankyou_info">

            <div class="col-sm-2"></div>
            <div class="col-sm-4 pay_left">Quantity: </div>
            <div class="col-sm-4 pay_right">{{$item->qty}}</div>
            <div class="col-sm-2"></div>

        </div><!-- .row -->

        <div class="row thankyou_info">

            <div class="col-sm-2"></div>
            <div class="col-sm-4 pay_left">Price: </div>
            <div class="col-sm-4 pay_right">$ {{$item->unit_price}}</div>
            <div class="col-sm-2"></div>
            <hr>

        </div><!-- .row -->

    <?php endforeach; ?>

    <div class="row thankyou_info">

        <h3>Payment Information:</h3>

        <div class="col-sm-2"></div>
        <div class="col-sm-4 pay_left">Subtotal: </div>
        <div class="col-sm-4 pay_right">$ {{$order->subtotal}}</div>
        <div class="col-sm-2"></div>

    </div><!-- .row -->

    <div class="row thankyou_info">

        <div class="col-sm-2"></div>
        <div class="col-sm-4 pay_left">GST/PST: </div>
        <div class="col-sm-4 pay_right">$ {{$order->taxes}}</div>
        <div class="col-sm-2"></div>

    </div><!-- .row -->

    <div class="row thankyou_info">

        <div class="col-sm-2"></div>
        <div class="col-sm-4 pay_left">Total: </div>
        <div class="col-sm-4 pay_right">$ {{$order->total_price}}</div>
        <div class="col-sm-2"></div>

    </div><!-- .row -->

</div> <!-- /thankyou -->

@endsection