@extends('layouts.app')

@section('content')

<div class="row"><!-- carousel -->

	<div class="padding-zero col-sm-12">

		<div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">

			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel" data-slide-to="0" class="active"></li>
				<li data-target="#carousel" data-slide-to="1"></li>
				<li data-target="#carousel" data-slide-to="2"></li>
				<li data-target="#carousel" data-slide-to="3"></li>
				<li data-target="#carousel" data-slide-to="4"></li>
				<li data-target="#carousel" data-slide-to="5"></li>
				<li data-target="#carousel" data-slide-to="6"></li>
			</ol><!-- /.carousel-indicators -->

			<!-- Wrapper for slides -->

			<div class="carousel-inner" role="listbox">

				<div class="item active">

					<img src="images/slider/slider6.jpg" alt="Watch Factory" />
					<div class="overlay"></div>
					<div class="carousel-caption">
						<h2>Creating Love</h2>
						<p>Our love and passion in a factory</p>
					</div>

				</div><!-- /.item active -->

				<div class="item">

					<img src="images/slider/slider2.jpg" alt="Transparent" />
					<div class="overlay"></div>
					<div class="carousel-caption">
						<h2>Blue is the Hot Color</h2>
						<p>Blue Trending</p>
					</div>

				</div><!-- /.item -->

				<div class="item">

					<img src="images/slider/slider3.jpg" alt="Multiple Options" />
					<div class="overlay"></div>
					<div class="carousel-caption">
						<h2>Multiple Options</h2>
						<p>Just because</p>
					</div>

				</div><!-- /.item -->

				<div class="item">

					<img src="images/slider/slider4.jpg" alt="Watches For Her" />
					<div class="overlay"></div>
					<div class="carousel-caption">
						<h2>Time for Diamonds</h2>
						<p>New diamond collection</p>
					</div>

				</div><!-- /.item -->

				<div class="item">

					<img src="images/slider/slider5.jpg" alt="Watch Mountain" />
					<div class="overlay"></div>
					<div class="carousel-caption">
						<h2>Adventure Collection</h2>
						<p>The whole world to conquer</p>
					</div>

				</div><!-- /.item -->

				<div class="item">

					<img src="images/slider/slider1.jpg" alt="Steel Blue" />
					<div class="overlay"></div>
					<div class="carousel-caption">
						<h2>Arctic Watches</h2>
						<p>The art and passion in your pulse</p>
					</div>

				</div><!-- /.item -->

				<div class="item">

					<img src="images/slider/slider7.jpg" alt="Black Watch" />
					<div class="overlay"></div>
					<div class="carousel-caption">
						<h2>Pure Black</h2>
						<p>Tradition and classic</p>
					</div>

				</div><!-- /.item -->

			</div><!-- /.carousel-inner -->

			<!-- Controls -->
			<a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>

		</div><!-- /#carousel -->

	</div><!-- /.col-lg-12 -->

</div><!-- /.row -->

<div class="center-block row"><!-- section1 -->

	<div class="section col-sm-6">
		<img src="images/arctic_watches.jpg" alt="silver watch" />
	</div><!-- /.col-xs-6 -->

	<div class="col-sm-6">

		    <h3>Arctic Watches</h3>
			<p>Proin tristique lectus lacinia, hendrerit eros vel, condimentum nulla. Fusce pellentesque rhoncus ante, et aliquet mauris rhoncus vitae. Aenean nec nunc condimentum, gravida nunc in, bibendum odio. In ultricies mi ex, non hendrerit eros dapibus posuere. Nam vel magna ultricies, scelerisque tellus vel, imperdiet metus. Sed vulputate, elit a faucibus laoreet, risus neque tristique augue, id gravida ligula nunc sed urna. Quisque semper commodo pellentesque. Integer efficitur eros egestas, tincidunt nulla eget, finibus turpis. Sed efficitur eget nunc id porttitor. Nullam fermentum euismod tortor maximus finibus.</p>

	</div><!-- /.col-xs-6 -->

</div><!-- /.center-block row -->

<div class="row"><!-- multiCarousel1 -->

	<div class="padding-zero col-sm-2"></div>

	<div class="col-sm-8">

		<div>
			<h3>Trending Collection</h3>
		</div>
	
		<div id="MultiCarousel1" class="MultiCarousel" data-items="2,3,3,4" data-slide="1" data-interval="1000">

			<div class="MultiCarousel-inner">

				@foreach($trending->watches as $trending_watch)
					<div class="item">
						<div class="pad15">
							<a href="/watch_detail/{{$trending_watch->id}}">
								<img src="images/Images/{{ $trending_watch->image }}" alt="watch" />
							</a>
							<h2>{{ $trending_watch->brand }}</h2>
						</div>
					</div><!-- /.item -->
				@endforeach

			</div><!-- /.MultiCarousel-inner -->

			<button class="btn btn-primary leftLst" type="button"><i class="fa fa-backward" aria-hidden="true"></i></button>
			<button class="btn btn-primary rightLst" type="button"><i class="fa fa-forward" aria-hidden="true"></i></button>

		</div><!-- /#MultiCarousel -->

	</div><!-- /.col-sm-8 -->

	<div class="padding-zero col-sm-2"></div>

</div><!-- /.row multiCarousel1 -->

<div class="row"><!-- multiCarousel2 -->

	<div class="padding-zero col-sm-2"></div>

	<div class="col-sm-8">

		<div>
			<h3>Sports Collection</h3>
		</div>

		<div id="MultiCarousel2" class="MultiCarousel" data-items="2,3,3,4" data-slide="1" data-interval="1000">

			<div class="MultiCarousel-inner">

				@foreach($sports->watches as $sport_watch)
					<div class="item">
						<div class="pad15">
							<a href="/watch_detail/{{$sport_watch->id}}">
								<img src="images/Images/{{ $sport_watch->image }}" alt="watch" />
							</a>
							<h2>{{ $sport_watch->brand }}</h2>
						</div>
					</div><!-- /.item -->
				@endforeach

			</div>

			<button class="btn btn-primary leftLst" type="button"><i class="fa fa-backward" aria-hidden="true"></i></button>
			<button class="btn btn-primary rightLst" type="button"><i class="fa fa-forward" aria-hidden="true"></i></button>

		</div><!-- /#MultiCarousel -->

	</div><!-- /.col-sm-8 -->

	<div class="padding-zero col-sm-2"></div>

</div><!-- /.row multiCarousel2-->

<div id="feature" class="row"><!-- featured images -->

	<div class="features padding-zero col-sm-6">

		<img src="images/lux_watch_header.jpg" alt="watch" />
		
		<div class="text_overlay">
			<div class="content">
				<div class="table">
					<div class="cell">
						<div class="inner">
							<h2>Blue Boat</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac velit posuere ipsum convallis congue.</p>
						</div><!-- /.inner -->
					</div><!-- /.cell -->
				</div><!-- /.table -->
			</div><!-- /.content -->
		</div><!-- /.text_overlay -->

	</div><!-- /.col-sm-6 -->

	<div class="padding-zero col-sm-6">

		<div class="row">

			<div class="features padding-zero col-sm-6">

				<img src="images/boat2.jpg" alt="watch" />

				<div class="text_overlay">
					<div class="content">
						<div class="table">
							<div class="cell">
								<div class="inner">
									<h2>Sailor</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac velit posuere ipsum convallis congue.</p>
								</div><!-- /.inner -->
							</div><!-- /.cell -->
						</div><!-- /.table -->
					</div><!-- /.content -->
				</div><!-- /.text_overlay -->

			</div><!-- /.col-sm-6 -->

			<div class="features padding-zero col-sm-6">

				<img src="images/bridge.jpg" alt="watch" />

				<div class="text_overlay">
					<div class="content">
						<div class="table">
							<div class="cell">
								<div class="inner">
									<h2>Bridge</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac velit posuere ipsum convallis congue.</p>
								</div><!-- /.inner -->
							</div><!-- /.cell -->
						</div><!-- /.table -->
					</div><!-- /.content -->
				</div><!-- /.text_overlay -->

			</div><!-- /.col-sm-6 -->

		</div><!-- /.row -->

		<div class="row">

			<div class="features padding-zero col-sm-6">

				<img src="images/female.jpg" alt="watch" />

				<div class="text_overlay">
					<div class="content">
						<div class="table">
							<div class="cell">
								<div class="inner">
									<h2>Magic</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac velit posuere ipsum convallis congue.</p>
								</div><!-- /.inner -->
							</div><!-- /.cell -->
						</div><!-- /.table -->
					</div><!-- /.content -->
				</div><!-- /.text_overlay -->

			</div><!-- /.col-sm-6 -->

			<div class="features padding-zero col-sm-6">

				<img src="images/astrolabe.jpg" alt="watch" />

				<div class="text_overlay">
					<div class="content">
						<div class="table">
							<div class="cell">
								<div class="inner">
									<h2>Astrolabe</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac velit posuere ipsum convallis congue.</p>
								</div><!-- /.inner -->
							</div><!-- /.cell -->
						</div><!-- /.table -->
					</div><!-- /.content -->
				</div><!-- /.text_overlay -->

			</div><!-- /.col-sm-6 -->

		</div><!-- /.row -->

	</div><!-- /.col-xs-6 -->

</div><!-- /.row -->

<div class="testemonial center-block row"><!-- /Testemonial -->

	<div class="col-sm-4">
		<div class="inner">
			<hr>
		</div><!-- /.inner -->
	</div><!-- /.col-sm-4 -->

	<div class="col-sm-4">
		<p>"Maecenas facilisis risus vel porttitor posuere. Quisque quis auctor eros. Maecenas iaculis, tortor nec eleifend finibus, lectus dolor mollis purus, at molestie velit nulla nec mi."</p>
		<p>Sir Steve George, company founder, 2017</p>
	</div><!-- /.col-sm-4 -->

	<div class="col-sm-4">
		<div class="inner">
			<hr>
		</div><!-- /.inner -->
	</div><!-- /.col-sm-4 -->

</div><!-- /.testemonial center-block row -->

@endsection