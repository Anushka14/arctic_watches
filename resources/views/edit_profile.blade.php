@extends('layouts.app')

@section('content')

<div id="edit-profile" class="row">

    <div class="bg-img col-sm-12">

        <div class="row">

            <div class="profile-box profile_sidebar col-sm-3">

                <div id="panel-heading" class="panel-heading">
                    <h2>Account Information</h2>
                </div>

                <div id="nav-sidebar-profile">
                    <ul>
                        <li><a href="/profile">Profile</a></li>
                        <li class="active_profile"><a href="/edit_profile">Edit Profile</a></li>
                        <li><a href="/change_password">Change Password</a></li>
                    </ul>
                </div><!-- /.nav-sidebar-profile -->

            </div><!-- /profile-box profile_sidebar col-sm-4 -->

            <div class="profile-box profile_sidebar col-sm-9">

                <div id="panel-heading" class="panel-heading">
                    <h2>Edit Profile</h2>
                </div>

                <div id="edit-profile-h4"><h4>Adjust profile fields below</h4></div>

                {!! Form::model($user,['url'=>'/profile', 'method'=>'PATCH']) !!}

                {{ csrf_field() }}

                <div class="profile_box-data">

                    <div class="edit-profile_detail">
                        <div class="edit_profile_form">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name') !!}
                        </div>
                    </div>

                    <div class="edit-profile_detail">
                        <div class="edit_profile_form">
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::text('email') !!}
                        </div>
                    </div>

                    <div class="edit-profile_detail">
                        <div class="edit_profile_form">
                        {!! Form::label('username', 'Username') !!}
                        {!! Form::text('username') !!}
                        </div>
                    </div>

                    <div class="edit-profile_detail">
                        <div class="edit_profile_form">
                        {!! Form::label('postal', 'Postal Code') !!}
                        {!! Form::text('postal') !!}
                        </div>
                    </div>

                    <div class="edit-profile_detail">
                        <div class="edit_profile_form">
                        {!! Form::label('street', 'Street') !!}
                        {!! Form::text('street') !!}
                        </div>
                    </div>

                    <div class="edit-profile_detail">
                        <div class="edit_profile_form">
                        {!! Form::label('city', 'City') !!}
                        {!! Form::text('city') !!}
                        </div>
                    </div>

                    <div class="edit-profile_detail">
                        <div class="edit_profile_form">
                        {!! Form::label('province', 'Province') !!}
                        {!! Form::text('province') !!}
                        </div>
                    </div>

                    <div class="edit-profile_detail">
                        <div class="edit_profile_form">
                        {!! Form::label('country', 'Country') !!}
                        {!! Form::text('country') !!}
                        </div>
                    </div>

                </div><!-- /.profile_box -->

                <!-- Update profile button -->
                <div class="form-group">
                    <input
                        type="hidden"
                        name="id"
                        value="<?php if(isset($clean['id'])) { echo $clean['id']; } ?>"/>
                    <button type="submit" class="btn btn-primary">Update Profile</button>
                </div>

                {!! Form::close() !!}

            </div><!-- /.profile-box profile_sidebar col-sm-9 -->

        </div><!-- /#form-box row -->

    </div><!-- /.background-img col-sm-12 -->

</div><!-- /#register .row -->
@endsection
