@extends('layouts.app')

@section('content')

<div id="register" class="row">

    <div class="bg-img col-sm-12">

        <div class="row">

            <div id="form-box"  class="col-sm-8 col-sm-offset-2">

                <div id="panel-heading" class="panel-heading">Register</div>

                <form
                    id="form-box"
                    method="POST"
                    action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class="row field-form">

                        <!-- NAME -->
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-sm-4 control-label">Name</label>
                            <div class="col-sm-8">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Name -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- EMAIL -->
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-4 control-label">E-Mail Address</label>
                            <div class="col-sm-8">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Email -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- USERNAME -->
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-sm-4 control-label">Username</label>
                            <div class="col-sm-8">
                                <input id="username" type="text" class="form-control" name="username" required>
                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Username -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- STREET -->
                        <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                            <label for="street" class="col-sm-4 control-label">Street</label>
                            <div class="col-sm-8">
                                <input id="street" type="text" class="form-control" name="street" required>
                                @if ($errors->has('street'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('street') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Street -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- POSTAL -->
                        <div class="form-group{{ $errors->has('postal') ? ' has-error' : '' }}">
                            <label for="postal" class="col-sm-4 control-label">Postal Code</label>
                            <div class="col-sm-8">
                                <input id="postal" type="text" class="form-control" name="postal" required>
                                @if ($errors->has('postal'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('postal') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Postal Code -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- CITY -->
                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city" class="col-sm-4 control-label">City</label>
                            <div class="col-sm-8">
                                <input id="city" type="text" class="form-control" name="city" required>
                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group City -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- PROVINCE -->
                        <div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
                            <label for="province" class="col-sm-4 control-label">Province</label>
                            <div class="col-sm-8">
                                <input id="province" type="text" class="form-control" name="province" required>
                                @if ($errors->has('province'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('province') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Province -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- COUNTRY -->
                        <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                            <label for="country" class="col-sm-4 control-label">Country</label>
                            <div class="col-sm-8">
                                <input id="country" type="text" class="form-control" name="country" required>
                                @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Country -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- PASSWORD -->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-4 control-label">Password</label>
                            <div class="col-sm-8">
                                <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Password -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- CONFIRM PASSWORD -->
                        <div class="form-group">
                            <label for="password-confirm" class="col-sm-4 control-label">Confirm Password</label>
                            <div class="col-sm-8">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div><!-- /.form-group Confirm Password -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div><!-- /.form-group Register Button -->

                    </div><!-- /.row field-form -->

                </form>

            </div><!-- /.col-sm-8 col-sm-offset-2 -->

        </div><!-- /#form-box row -->

    </div><!-- /.background-img col-sm-12 -->

</div><!-- /#register .row -->
@endsection
