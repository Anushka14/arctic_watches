@extends('layouts.app')

@section('content')

<div id="login" class="row">

    <div class="bg-img col-sm-12">

        <div class="row">

            <div id="form-box"  class="col-sm-8 col-sm-offset-2">

                <div id="panel-heading" class="panel-heading">Login</div>

                <form
                    id="form-box"
                    method="POST"
                    action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="row field-form">

                        <!-- EMAIL -->
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-4 control-label">E-Mail Address</label>
                            <div class="col-sm-8">
                                <input
                                    id="email"
                                    type="email"
                                    class="form-control"
                                    name="email"
                                    placeholder="EMAIL"
                                    value="{{ old('email') }}"
                                    required
                                    autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Email -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- PASSWORD -->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-4 control-label">Password</label>
                            <div class="col-sm-8">
                                <input
                                    id="password"
                                    class="form-control"
                                    type="password"
                                    name="password"
                                    placeholder="PASSWORD"
                                    required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Password -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <div class="form-group">
                            <div class="checkbox col-sm-12">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                            </div><!-- /.checkbox col-sm-12 -->
                        </div><!-- /.form-group Login Button -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>
                            </div>
                        </div><!-- /.form-group Login Button -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <div class="col-sm-12">
                            <a class="btn btn-link" id="link" href="{{ route('password.request') }}">Forgot Your Password?</a>
                        </div><!-- /.col-sm-12 -->

                    </div><!-- /.row field-form -->

                </form>

            </div><!-- /.col-sm-8 col-sm-offset-2 -->

        </div><!-- /#form-box row -->

    </div><!-- /.background-img col-sm-12 -->

</div><!-- /#register .row -->
@endsection
