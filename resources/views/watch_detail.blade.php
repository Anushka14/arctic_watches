@extends('layouts.app')

@section('content')

<main id="detail">
		
	<div class="row"><!-- detail -->

		<div class="col-sm-2 hidden-xs"></div>

		<div class="padding-zero col-sm-4">

			<h2></h2>

			<div id="small-image1">
				<img src="/images/Images/{{$watch->image}}"/>
			</div>

		</div><!-- /.col-sm-4 -->

		<div class="padding-zero col-sm-4">

			<div class="detail-content">
				<hr>
					<div class="row">
						<div class="col-sm-6">
							<p id="content1">Brand: {{$watch->title}}</p>
							<p>sku:{{$watch->sku}}</p>

							<hr />
							<p id="content2">Price: ${{$watch->price}}</p>
							<p>Shipping Cost: ${{$watch->shipping_cost}}</p>
						</div>

						<div class="col-sm-6">
							<p id="content1">In Stock: {{$watch->instock}}</p>
							<p>Size:{{$watch->size}}</p>
							<hr/>

							<p>Length: {{$watch->length}}</p>
							<p>Weight: {{$watch->weight}}</p>
						</div>
						<hr/>
						<div class="short">
							<h2>SHORT DESCRIPTION</h2>
							<p>{{$watch->short_description}}</p>
						</div>
					</div>
				<hr>

				<form method="post"
						action="/add_watch"
						id="watch_detail"
						novalidate>
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					<input type="hidden" name="id" value="{{$watch->id}}" />
					<input type="hidden" name="name" value="{{$watch->title}}" />
					<input type="hidden" name="price" value="{{$watch->price}}" />
					<input type="hidden" name="image" value="{{$watch->image}}" />
					<input class="btn btn-primary" type="submit" value="ADD TO CART" />
				</form>

			</div>


		</div><!-- /.col-sm-4 -->

		<div class="col-sm-2 hidden-xs"></div>

	</div><!-- /.row detail -->

	<div class="row"><!-- detail -->

		<div class="col-sm-2 hidden-xs"></div>

		<div class="padding-zero col-sm-8">

			<h2>DESCRIPTION</h2>

			<div id="long-description">
				<p>{{$watch->long_description}}</p>
			</div>

		</div><!-- /.col-sm-8 -->

		<div class="col-sm-2 hidden-xs"></div>

	</div><!-- /.row detail -->

</main>
    
@endsection