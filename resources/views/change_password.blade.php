@extends('layouts.app')

@section('content')

<div id="edit-profile" class="row">

    <div class="bg-img col-sm-12">

        <div class="row">

            <div class="profile-box profile_sidebar col-sm-3">

                <div id="panel-heading" class="panel-heading">
                    <h2>Account Information</h2>
                </div>

                <div id="nav-sidebar-profile">
                    <ul>
                        <li><a href="/profile">Profile</a></li>
                        <li><a href="/edit_profile">Edit Profile</a></li>
                        <li class="active_profile"><a href="/change_password">Change Password</a></li>
                    </ul>
                </div><!-- /.nav-sidebar-profile -->

            </div><!-- /profile-box profile_sidebar col-sm-4 -->

            <div class="profile-box profile_sidebar col-sm-9">

                <div id="panel-heading" class="panel-heading">
                    <h2>Change Password</h2>
                </div>

                <div id="change-pwd-h4"><h4>Please fill out the fields below to change your password</h4></div>
                
                {!! Form::open(['url'=>'/change_password', 'method'=>'PATCH']) !!}
                   
                <div id="form-box-change">
                
                    <div class="row field-form">
                        {{ csrf_field() }}

                        <!-- NEW PASSWORD -->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                           
                            <label for="password" class="col-sm-4 control-label"><i class="fa fa-lock" aria-hidden="true"></i> New Password: </label>
                            
                            <div class="col-sm-8">
                                <input id="password" type="password" class="form-control" name="password" >
                                 @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                            </div><!-- /.col-sm-8 -->

                        </div><!-- /.form-group New Password -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- CONFIRM PASSWORD -->
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-4 control-label"><i class="fa fa-lock" aria-hidden="true"></i> Confirm New Password: </label>

                            <div class="col-sm-8">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" >
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                            </div><!-- /.col-sm-8 -->
                            
                        </div><!-- /.form-group Confirm Password -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" value="<?php if(isset($clean['id'])) { echo $clean['id']; } ?>"/>
                                <button  type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div><!-- /.form-group Register Button -->

                    </div><!-- /.row field-form -->

                </div><!-- /.form-box-change -->

                </form>

            </div><!-- /.profile-box profile_sidebar col-sm-9 -->

        </div><!-- /#form-box row -->

    </div><!-- /.background-img col-sm-12 -->

</div><!-- /#register .row -->
@endsection