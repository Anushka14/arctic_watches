@extends('layouts.app')

@section('content')

<div id="profile" class="row">

    <div class="bg-img col-sm-12">

        <div class="row">

            <div class="profile-box profile_sidebar col-sm-3">

                <div id="panel-heading" class="panel-heading">
                    <h2>Account Information</h2>
                </div>

                <div id="nav-sidebar-profile">
                    <ul>
                        <li class="active_profile"><a href="/profile">Profile</a></li>
                        <li><a href="/edit_profile">Edit Profile</a></li>
                        <li><a href="/change_password">Change Password</a></li>
                    </ul>
                </div><!-- /.nav-sidebar-profile -->

            </div><!-- /profile-box profile_sidebar col-sm-4 -->

            <div class="profile-box profile_sidebar col-sm-9">

                <div id="panel-heading" class="panel-heading">
                    <h2>Profile</h2>
                </div>

                <div class="profile_box-data">

                    <div class="profile_detail"><strong>Name: </strong>{{ $user->name }}</div>
                    <div class="profile_detail"><strong>E-Mail Address: </strong>{{ $user->email }}</div>
                    <div class="profile_detail"><strong>Username: </strong>{{ $user->username }}</div>
                    <div class="profile_detail"><strong>Street: </strong>{{ $user->street }}</div>
                    <div class="profile_detail"><strong>Postal Code: </strong>{{ $user->postal }}</div>
                    <div class="profile_detail"><strong>City: </strong>{{ $user->city }}</div>
                    <div class="profile_detail"><strong>Province: </strong>{{ $user->province }}</div>
                    <div class="profile_detail"><strong>Country: </strong>{{ $user->country }}</div>

                </div><!-- /.profile_box -->

            </div><!-- /.profile-box col-sm-9 -->

        </div><!-- /#form-box row -->

    </div><!-- /.background-img col-sm-12 -->

</div><!-- /#register .row -->
@endsection
