@extends('layouts.app')

@section('content')

<div class="row"><!-- featured images -->

	<div class="features-about col-sm-12">

		<div class="about-container">

		<img src="images/about_us_bridge.jpg" alt="watch" />
		
		<div class="text_overlay-about">
			<div class="content-about">
				<div class="table">
					<div class="cell">
						<div class="inner-about">
							<h2>20 years building valuable time</h2>
							<hr>
							<div>
								<p>Curabitur vitae ex vehicula, commodo enim tincidunt, facilisis diam. Nam condimentum molestie arcu, non tristique mauris iaculis non. Phasellus porta tempor quam, id bibendum sem dignissim nec. Nulla a magna massa. Nunc sollicitudin auctor leo, sed dapibus nisi aliquam nec.</p>
							</div>
						</div><!-- /.inner -->
					</div><!-- /.cell -->
				</div><!-- /.table -->
			</div><!-- /.content -->
		</div><!-- /.text_overlay -->

		</div>

	</div><!-- /.col-sm-12 -->

</div><!-- /.row -->

<div class="testemonial center-block row"><!-- /Testemonial -->

	<div class="col-sm-4">
		<div class="inner">
			<hr>
		</div><!-- /.inner -->
	</div><!-- /.col-sm-4 -->

	<div class="col-sm-4">
		<p>"Vivamus ut porta quam. Aenean mi lectus, malesuada ultrices nibh sit amet, lobortis mattis urna. Nulla at justo quis purus scelerisque hendrerit. Nulla fermentum dui a faucibus porttitor. Praesent mattis tortor nec orci fermentum, in posuere ipsum porta. Integer consectetur leo ullamcorper, laoreet leo quis, tristique enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed elementum metus at lorem elementum scelerisque."</p>
		<p>Sir Brent Scott, company friend, 2017</p>
	</div><!-- /.col-sm-4 -->

	<div class="col-sm-4">
		<div class="inner">
			<hr>
		</div><!-- /.inner -->
	</div><!-- /.col-sm-4 -->

</div><!-- /.testemonial center-block row -->

<div class="center-block row"><!-- text -->

	<div class="content-about-text col-sm-8 offset-sm-2">

		<p>Vestibulum finibus enim risus, id sollicitudin velit consectetur sit amet. Pellentesque quis congue diam. Mauris luctus augue at purus pretium, sit amet sagittis arcu ultricies. In consequat sollicitudin orci vitae auctor. Phasellus vel justo a tortor mollis egestas. Cras porta sit amet lectus a consequat. Maecenas ultrices ullamcorper nisl eget imperdiet. Mauris ullamcorper, mi et consectetur tempor, augue dui venenatis metus, in ullamcorper eros ex ac enim. Aenean finibus auctor ipsum in pulvinar. Integer dapibus, sapien ut viverra pharetra, felis diam dignissim mi, vitae egestas erat lectus porttitor lectus. Morbi non consectetur erat, vulputate lacinia magna. Nullam nisl lorem, ornare eu mauris at, ultricies pulvinar ex.</p>

		<p>Donec erat odio, laoreet ut mauris a, laoreet dictum dui. Fusce pharetra elit placerat condimentum vestibulum. Fusce ut nunc nisl. Integer at volutpat diam. Integer justo magna, accumsan a ultricies hendrerit, fringilla et ipsum. Aenean eros metus, pretium ut nunc non, bibendum auctor enim. In turpis leo, feugiat laoreet velit at, consequat ultricies urna. Phasellus vitae mauris non felis efficitur molestie ac quis nulla. Praesent velit enim, pulvinar mollis convallis in, elementum ac arcu. Vestibulum id gravida magna, at dapibus sapien. Fusce eu lectus non libero dapibus consectetur sed vel leo. Suspendisse ornare justo vitae risus interdum venenatis. Aliquam malesuada mollis ligula, nec blandit tellus placerat eu. Pellentesque accumsan malesuada mi sit amet faucibus. Nam nec est id metus pellentesque dignissim. Nulla finibus libero eget felis malesuada, vel pretium odio malesuada.</p>

		<p>Pellentesque et ultrices mauris. Nulla interdum lectus quis dolor pellentesque, et elementum libero semper. Morbi maximus eu dolor accumsan ultrices. Nullam pellentesque facilisis erat ornare dictum. Donec consequat velit eget condimentum posuere. Donec feugiat, nisl ac convallis pulvinar, lacus ex vehicula purus, ut cursus neque neque ac magna. Quisque finibus turpis leo, non commodo massa vestibulum ut. Aliquam erat volutpat. Proin dapibus condimentum sodales. Quisque libero augue, finibus sed urna at, rutrum sollicitudin augue.</p>

	</div><!-- /.col-sm-8 -->

</div><!-- /.row -->
    
@endsection
