@extends('layouts.app')

@section('content')

<div id="checkout">

	<div id="checkout_title" class="row">

		<div class="col-sm-12 shopp_line">
			<h1>Checkout</h1>
			<hr>
		</div>

	</div><!-- /#checkout_title row -->

	<form
		method="post"
		action="/confirm"
		id="payment_info"
		novalidate>

		<div id="shipping" class="row">

			<div class="col-sm-6 payment_line">
				<h2>Shipping</h2>
				<hr>

				<fieldset class="field">

					<div class="row"><!-- Shipping Address -->

						<div class="col-sm-6 pay_left">
							<label for="shipping_street" class="required">* Shipping Address:</label>
						</div><!-- /.col-sm-6 pay_left -->

						<div class="col-sm-6 pay_right">
							<input
								type="text"
								name="shipping_street"
								id="shipping_street"
								class="pay_right"
								maxlength="120"
								placeholder="Type the street address"
								tabindex="1"
								required
								value="{{ old('shipping_street') }}"/>
							<span class="error">
								@if ($errors->has('shipping_street'))
									{{ $errors->first('shipping_street') }}
								@endif
							</span>
						</div><!-- /.col-sm-6 pay_right -->

					</div><!-- /.row Shipping Address -->

					<div class="row"><!-- Shipping Postal Code -->

						<div class="col-sm-6 pay_left">
							<label for="shipping_postal" class="required">* Postal Code:</label>
						</div><!-- /.col-sm-6 pay_left -->

						<div class="col-sm-6 pay_right">
							<input
								type="text"
								name="shipping_postal"
								id="shipping_postal"
								class="pay_right"
								maxlength="120"
								placeholder="Type the postal code"
								tabindex="1"
								required
								value="{{ old('shipping_postal') }}"/>
							<span class="error">
								@if ($errors->has('shipping_postal'))
									{{ $errors->first('shipping_postal') }}
								@endif
							</span>
						</div><!-- /.col-sm-6 pay_right -->

					</div><!-- /.row Shipping Postal Code -->

					<div class="row"><!-- Shipping City -->

						<div class="col-sm-6 pay_left">
							<label for="shipping_city" class="required">* City:</label>
						</div><!-- /.col-sm-6 pay_left -->

						<div class="col-sm-6 pay_right">
							<input
								type="text"
								name="shipping_city"
								id="shipping_city"
								class="pay_right"
								maxlength="120"
								placeholder="Type the city"
								tabindex="1"
								required
								value="{{ old('shipping_city') }}"/>
							<span class="error">
								@if ($errors->has('shipping_city'))
									{{ $errors->first('shipping_city') }}
								@endif
							</span>
						</div><!-- /.col-sm-6 pay_right -->

					</div><!-- /.row Shipping City -->

					<div class="row"><!-- Shipping Province -->

						<div class="col-sm-6 pay_left">
							<label for="shipping_province" class="required">* Province:</label>
						</div><!-- /.col-sm-6 pay_left -->

						<div class="col-sm-6 pay_right">
							<input
								type="text"
								name="shipping_province"
								id="shipping_province"
								class="pay_right"
								maxlength="120"
								placeholder="Type the province"
								tabindex="1"
								required
								value="{{ old('shipping_province') }}"/>
							<span class="error">
								@if ($errors->has('shipping_province'))
									{{ $errors->first('shipping_province') }}
								@endif
							</span>
						</div><!-- /.col-sm-6 pay_right -->

					</div><!-- /.row Shipping Province -->

					<div class="row"><!-- Shipping Country -->

						<div class="col-sm-6 pay_left">
							<label for="shipping_country" class="required">* Country:</label>
						</div><!-- /.col-sm-6 pay_left -->

						<div class="col-sm-6 pay_right">
							<input
								type="text"
								name="shipping_country"
								id="shipping_country"
								class="pay_right"
								maxlength="120"
								placeholder="Type the country"
								tabindex="1"
								required
								value="{{ old('shipping_country') }}"/>
							<span class="error">
								@if ($errors->has('shipping_country'))
									{{ $errors->first('shipping_country') }}
								@endif
							</span>
						</div><!-- /.col-sm-6 pay_right -->

					</div><!-- /.row Shipping Country -->

				</fieldset> <!-- /fieldset -->

			</div><!-- /.col-sm-6 payment_line -->

			<div class="col-sm-6 payment_line">
				<h2>Payment</h2>
				<hr>

				<fieldset class="field">

					<div class="row">
						<div class="col-sm-12">
							<p><span class="error">*</span> Required fields</p>
						</div>
					</div><!-- /.row -->

					<div class="row"><!-- Card Type -->

						<div class="col-sm-6 pay_left">
							<label for="card_type" class="required">* Card Type:</label>
						</div><!-- /.col-sm-6 pay_left -->

						<div class="col-sm-6 pay_right">
							<select id="card_type" class="pay_right" name="card_type">
								<option>Visa</option>
								<option>Mastercard</option>
								<option>American Express</option>
								<option>Diners Club</option>
								<option>Discover</option>
								<option>JCB</option>
							</select>
							<span class="error">
								@if ($errors->has('card_type'))
									{{ $errors->first('card_type') }}
								@endif
							</span>
						</div><!-- /.col-sm-6 pay_right -->

					</div><!-- /.row Card Type -->

					<div class="row"><!-- Name on Card -->

						<div class="col-sm-6 pay_left">
							<label for="card_name" class="required">* Name on Card:</label>
						</div><!-- /.col-sm-6 pay_left -->

						<div class="col-sm-6 pay_right">
							<input
								type="text"
								name="card_name"
								id="card_name"
								class="pay_right"
								maxlength="120"
								placeholder="Type name on card"
								tabindex="1"
								required
								value="{{ old('card_name') }}"/>
							<span class="error">
								@if ($errors->has('card_name'))
									{{ $errors->first('card_name') }}
								@endif
							</span>
						</div><!-- /.col-sm-6 pay_right -->

					</div><!-- /.row Name on Card -->

					<div class="row"><!-- Card Number -->

						<div class="col-sm-6 pay_left">
							<label for="card_number" class="required">* Card Number:</label>
						</div><!-- /.col-sm-6 pay_left -->

						<div class="col-sm-6 pay_right">
							<input
								type="text"
								name="card_number"
								id="card_number"
								class="pay_right"
								maxlength="16"
								placeholder="Type card number"
								tabindex="1"
								required
								value="{{ old('card_number') }}"/>
							<span class="error">
								@if ($errors->has('card_number'))
									{{ $errors->first('card_number') }}
								@endif
							</span>
						</div><!-- /.col-sm-6 pay_right -->

					</div><!-- /.row Card Number -->

					<div class="row"><!-- Expiry Date -->

						<div class="col-sm-6 pay_left">
							<label for="card_expire" class="required">* Expiry Date:</label>
						</div><!-- /.col-sm-6 pay_left -->

						<div class="col-sm-6 pay_right">
							<input
								type="text"
								name="card_expire"
								id="card_expire"
								class="pay_right"
								maxlength="7"
								placeholder=" mmyy"
								tabindex="1"
								required
								value="{{ old('card_expire') }}"/>
							<span class="error">
								@if ($errors->has('card_expire'))
									{{ $errors->first('card_expire') }}
								@endif
							</span>
						</div><!-- /.col-sm-6 pay_right -->

					</div><!-- /.row Expiry Date -->

					<div class="row"><!-- Security Number -->

						<div class="col-sm-6 pay_left">
							<label for="card_cvv" class="required">* Security Number:</label>
						</div><!-- /.col-sm-6 pay_left -->

						<div class="col-sm-6 pay_right">
							<input
								type="text"
								name="card_cvv"
								id="card_cvv"
								class="pay_right"
								maxlength="4"
								placeholder="3 or 4 digits"
								tabindex="1"
								required
								value="{{ old('card_cvv') }}"/>
							<span class="error">
								@if ($errors->has('card_cvv'))
									{{ $errors->first('card_cvv') }}
								@endif
							</span>
						</div><!-- /.col-sm-6 pay_right -->

					</div><!-- /.row Security Number -->

					<div class="row">

						<div class="error col-sm-12">
							@if(session()->has('error'))
							    <div class="alert alert-{{ session('error') }}"> 
							    	{!! session('error') !!}
							    </div>
							@endif
						</div><!-- /.col-sm-6 pay_left -->

					</div><!-- /.row -->

					

				</fieldset> <!-- /fieldset -->

			</div><!-- /.col-sm-6 payment_line -->

		</div><!-- /#shipping .row -->

		<div class="row">

			<hr>

			<div class="col-sm-12 cart_button">
				<input class="btn btn-primary" type="reset" value="Clear" /> &nbsp;
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
				<input class="btn btn-primary" type="submit" value="Complete Payment" />
			</div>

		</div><!-- .row -->
		
	</form>  <!-- /form -->

</div><!-- /#checkout -->

@endsection