<!DOCTYPE html>
<html lang="en">
<head>

    <title>
        @if(!empty($title))
            {{ $title }}
        @else
            Arctic Watches
        @endif
    </title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, inital-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- link for fonts -->
    <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400i%7cRoboto:400,400i" rel="stylesheet">
    <!-- link for style css -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="/css/style.css" type="text/css" />

</head>

<body>
<div class="container-fluid">

    <h1>
        @if(!empty($title))
            {{ $title }}
        @else
            Arctic Watches
        @endif
    </h1>

    <div id="main_header">

        <div id="header" class="row"><!-- header -->

            <div class="col-sm-3"></div>

            <div id="logo" class="col-sm-6 navbar-nav-logo">
                <a href="/"><img src="/images/Logo.png" alt="arctic logo" /></a>
            </div><!-- /#logo -->

            <div id="search" class="col-sm-3">

                <form action="/search" method="GET" class="navbar-form navbar-center" role="search">

                    <div class="input-group search_bx pull-right">
							<span class="input-group-btm">
								<button class="btn btn-default" type="submit">
									<i class="fa fa-search" aria-hidden="true"></i>
								</button>
								<input type="text" name="term" id="term" class="form-control">
							</span>
                    </div><!-- /.input-group -->

                </form>

            </div><!-- /#search -->

        </div><!-- /#header .row -->

        <div class="row"><!-- navbar -->

            <nav class="navbar navbar-inverse col-xs-12">

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div><!-- /.navbar-header -->

                <div id="myNavbar" class="collapse navbar-collapse navbar-nav-main">

                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/">Back to Store</a></li>
                        <li class="active"><a href="/admin">Dashboard</a></li>
                        <li class="active"><a href="/admin/watches">Watches</a></li>
                        <li class="active"><a href="/admin/orders">Orders</a></li>
                        <li class="active"><a href="/admin/tag">Tags</a></li>
                        <li class="active"><a href="/admin/category">Categories</a></li>
                        <li class="active"><a href="/admin/admin_users/index">Users</a></li>

                    </ul>

                </div><!-- /#myNavbar -->

            </nav><!-- /.navbar navbar-inverse -->

        </div><!-- /.row -->

    </div><!-- /#main_header -->

    <div>
        @yield('content')
    </div>

    <footer>

        <div class="row">

            <div class="col-lg-12">

                <div class="row">

                    <div id="footer_left" class="col-sm-4">

                        <h4>THE COLLECTION</h4>
                        <hr>

                        <ul>
                            <li><a href="#">ROLEX</a></li>
                            <li><a href="#">OMEGA</a></li>
                            <li><a href="#">TISSOT</a></li>
                            <li><a href="#">AUDEMARS PIGUET</a></li>
                            <li><a href="#">PATEK PHILLIPPE</a></li>
                            <li><a href="#">CHOPARD</a></li>
                            <li><a href="#">BREITLING</a></li>
                            <li><a href="#">BREMONT</a></li>
                            <li><a href="#">TAG HEUER</a></li>
                            <li><a href="#">MAURICE LACROIX</a></li>
                            <li><a href="#">MONTBLANC</a></li>
                            <li><a href="#">TUDOR</a></li>
                            <li><a href="#">HARRY WINSTON</a></li>
                            <li><a href="#">BVLGARI</a></li>
                        </ul>

                    </div><!-- /.col-sm-4 -->

                    <div id="footer_middle" class="col-sm-4">

                        <h4>OUR CHANNELS</h4>
                        <hr>

                        <ul>
                            <li><a href="#">FACEBOOK</a></li>
                            <li><a href="#">INSTAGRAM</a></li>
                            <li><a href="#">YOUTUBE</a></li>
                            <li><a href="#">PINTEREST</a></li>
                        </ul>

                        <div class="logo">
                            <img src="images/Logo.png" alt="logo" />
                        </div>

                    </div><!-- /#row2 .col-sm-4 -->

                    <div class="col-sm-4">

                        <div id="footer_right">

                            <div>
                                <h4>LEGAL NOTICES</h4>
                                <hr>

                                <ul>
                                    <li><a href="#">TERMS OF USE</a></li>
                                    <li><a href="#">LEGAL NOTICE</a></li>
                                    <li><a href="#">COOKIE POLICY</a></li>
                                </ul>
                            </div>

                            <div class="right_col">

                                <h4>CONTACT US</h4>
                                <hr>

                                <h5>ADDRESS:</h5>
                                <p>47938 37th. Str. NEW YORK</p>
                                <h5>EMAIL:</h5>
                                <p>arctic.watches@gmail.com</p>
                                <h5>TELEPHONE:</h5>
                                <p>1(800)738-9330</p>

                            </div>

                        </div><!-- /#footer_right -->

                    </div><!-- /.col-sm-4 -->

                </div>

            </div>

        </div><!-- /.row -->

        <div class="row copyright">
            <div class="col-sm-12">
                <p>Contents copyright &copy; 2017 - by Marvel7 Partners</p>
            </div>
        </div>

    </footer>

</div><!-- /.container-fluid -->

<!-- link for js -->
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script src="/js/carousel.js"></script>
<script src="/js/active.js"></script>

</body>
</html>