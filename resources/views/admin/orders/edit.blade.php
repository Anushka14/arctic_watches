@extends('layouts.admin')

@section('content')

<div id="edit-admin-order">

    <div class="row">

        <div class="watches_bar col-sm-12">

            <nav class="navbar navbar-inverse">
                <ul class="nav navbar-nav">
                    <li>Edit Orders</li>
                </ul>
            </nav>

        </div><!-- /.watches_bar .col-sm-12 -->

    </div><!-- /.row -->

    <div class="row">

        <div class="profile-box profile_sidebar col-sm-12">

            {!! Form::model($order,['url'=>'/admin/orders', 'method'=>'PATCH']) !!}

            {{ csrf_field() }}

            <div class="profile_box-data">

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('user_id', 'user_id') !!}
                    {!! Form::text('user_id') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('name', 'name') !!}
                    {!! Form::text('name') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('email', 'email') !!}
                    {!! Form::text('email') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('username', 'username') !!}
                    {!! Form::text('username') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('street', 'street') !!}
                    {!! Form::text('street') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('postal', 'postal') !!}
                    {!! Form::text('postal') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('city', 'city') !!}
                    {!! Form::text('city') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('province', 'province') !!}
                    {!! Form::text('province') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('country', 'country') !!}
                    {!! Form::text('country') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('subtotal', 'subtotal') !!}
                    {!! Form::text('subtotal') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('taxes', 'taxes') !!}
                            {!! Form::text('taxes') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('total_price', 'total_price') !!}
                    {!! Form::text('total_price') !!}
                    </div>
                </div>

                <!-- <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('shipping_address', 'shipping_address') !!}
                    {!! Form::text('shipping_address') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('transaction_status', 'transaction_status') !!}
                    {!! Form::text('transaction_status') !!}
                    </div>
                </div> -->

            </div><!-- /.profile_box -->

            <!-- Update profile button -->
            <div class="form-group">
                {!! Form::hidden('id') !!}
                <button type="submit" class="btn btn-primary">Update Order</button>
            </div>

            {!! Form::close() !!}

        </div><!-- /.profile-box profile_sidebar col-sm-9 -->

    </div><!-- /row -->

</div><!-- /#edit-admin -->
@endsection