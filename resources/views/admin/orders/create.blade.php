@extends('layouts.admin')

@section('content')

    <div class="col-sm-10">
        {{var_dump($errors->all())}}
        <form class="form-horizontal" method="POST" action="/admin/orders">
        {{ csrf_field() }}

            <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                <label for="user_id" class="col-md-4 control-label">user_id</label>

                <div class="col-md-6">
                    <input id="user_id" type="number" class="form-control"
                           name="user_id" value="{{ old('user_id') }}" required autofocus>

                    @if ($errors->has('user_id'))
                        <span class="help-block">
                        <strong>{{ $errors->first('user_id') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">name</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control"
                           name="name" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">email</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control"
                           name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username" class="col-md-4 control-label">username</label>

                <div class="col-md-6">
                    <input id="username" type="text" class="form-control"
                           name="username" value="{{ old('username') }}" required autofocus>

                    @if ($errors->has('username'))
                        <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                <label for="street" class="col-md-4 control-label">street</label>

                <div class="col-md-6">
                    <input id="street" type="text" class="form-control"
                           name="street" value="{{ old('street') }}" required autofocus>

                    @if ($errors->has('street'))
                        <span class="help-block">
                        <strong>{{ $errors->first('street') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('postal') ? ' has-error' : '' }}">
                <label for="postal" class="col-md-4 control-label">postal</label>

                <div class="col-md-6">
                    <input id="postal" type="text" class="form-control"
                           name="postal" value="{{ old('postal') }}" required autofocus>

                    @if ($errors->has('postal'))
                        <span class="help-block">
                        <strong>{{ $errors->first('postal') }}</strong>
                    </span>
                    @endif
                </div>
            </div>



            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                <label for="city" class="col-md-4 control-label">city</label>

                <div class="col-md-6">
                    <input id="city" type="text" class="form-control"
                           name="city" value="{{ old('city') }}" required autofocus>

                    @if ($errors->has('city'))
                        <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
                <label for="province" class="col-md-4 control-label">province</label>

                <div class="col-md-6">
                    <input id="province" type="text" class="form-control"
                           name="province" value="{{ old('province') }}" required autofocus>

                    @if ($errors->has('province'))
                        <span class="help-block">
                        <strong>{{ $errors->first('province') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                <label for="country" class="col-md-4 control-label">country</label>

                <div class="col-md-6">
                    <input id="country" type="text" class="form-control"
                           name="country" value="{{ old('country') }}" required autofocus>

                    @if ($errors->has('country'))
                        <span class="help-block">
                        <strong>{{ $errors->first('country') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('subtotal') ? ' has-error' : '' }}">
                <label for="subtotal" class="col-md-4 control-label">subtotal</label>

                <div class="col-md-6">
                    <input id="subtotal" type="number" class="form-control"
                           name="subtotal" value="{{ old('subtotal') }}" required autofocus>

                    @if ($errors->has('subtotal'))
                        <span class="help-block">
                        <strong>{{ $errors->first('subtotal') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('taxes') ? ' has-error' : '' }}">
                <label for="taxes" class="col-md-4 control-label">taxes</label>

                <div class="col-md-6">
                    <input id="taxes" type="number" class="form-control"
                           name="taxes" value="{{ old('taxes') }}" required autofocus>

                    @if ($errors->has('taxes'))
                        <span class="help-block">
                        <strong>{{ $errors->first('taxes') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('total_price') ? ' has-error' : '' }}">
                <label for="total_price" class="col-md-4 control-label">total_price</label>

                <div class="col-md-6">
                    <input id="total_price" type="number" class="form-control"
                           name="total_price" value="{{ old('total_price') }}" required autofocus>

                    @if ($errors->has('total_price'))
                        <span class="help-block">
                        <strong>{{ $errors->first('total_price') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

          <!--   <div class="form-group{{ $errors->has('shipping_address') ? ' has-error' : '' }}">
                <label for="shipping_address" class="col-md-4 control-label">shipping_address</label>

                <div class="col-md-6">
                    <input id="shipping_address" type="text" class="form-control"
                           name="shipping_address" value="{{ old('shipping_address') }}" required autofocus>

                    @if ($errors->has('shipping_address'))
                        <span class="help-block">
                        <strong>{{ $errors->first('shipping_address') }}</strong>
                    </span>
                    @endif
                </div>
            </div> -->

            <!-- <div class="form-group{{ $errors->has('transaction_status') ? ' has-error' : '' }}">
                <label for="transaction_status" class="col-md-4 control-label">transaction_status</label>

                <div class="col-md-6">
                    <input id="transaction_status" type="text" class="form-control"
                           name="transaction_status" value="{{ old('transaction_status') }}" required autofocus>

                    @if ($errors->has('transaction_status'))
                        <span class="help-block">
                        <strong>{{ $errors->first('transaction_status') }}</strong>
                    </span>
                    @endif
                </div>
            </div> -->



            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Submit
                    </button>
                </div>
            </div>
        </form>
     </div>
@endsection
