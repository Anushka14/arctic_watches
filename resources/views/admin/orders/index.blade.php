@extends('layouts.admin')

@section('content')

<div id="orders-watch-admin" class="row">

    <div class="watches_bar col-sm-12">

        <nav class="navbar navbar-inverse">
            <!-- <div class="navbar-header">
                <a class="navbar-brand" href="{{ URL::to('admin/index') }}">Watches</a>
            </div> -->
            <ul class="nav navbar-nav">
                <!-- <li><a href="{{ URL::to('admin/index') }}">View All Watches</a></li> -->
                <li><a href="{{ URL::to('admin/orders/create') }}">Create an Order</a>
            </ul>
        </nav>

    </div><!-- /.col-sm-12 -->

        <h1>All the Watches</h1>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>user_id</td>
                <td>name</td>
                <td>email</td>
                <td>username</td>
                <td>street</td>
                <td>postal</td>
                <td>city</td>
                <td>province</td>
                <td>country</td>
                <td>subtotal</td>
                <td>taxes</td>
                <td>total_price</td>
                <td>transaction_status</td>
            </tr>
        </thead>
        <tbody>
        @foreach($orders as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->user_id }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->email }}</td>
                <td>{{ $value->username }}</td>
                <td>{{ $value->street }}</td>
                <td>{{ $value->postal }}</td>
                <td>{{ $value->city }}</td>
                <td>{{ $value->province }}</td>
                <td>{{ $value->country }}</td>
                <td>{{ $value->subtotal }}</td>
                <td>{{ $value->taxes }}</td>
                <td>{{ $value->total_price }}</td>
                <td>{{ $value->transaction_status }}</td>
                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    <a class="btn btn-small btn-info" href="{{ URL::to('admin/order/' . $value->id .
                     '/edit') }}">Edit this order</a>
                  
                    <!--added to test delete on different route-->
                    <a class="btn btn-small btn-danger" href="/admin/orders/delete/{{ $value->id }}">
                        Delete this order
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{$orders->links()}}

</div><!-- /#orders-watch-admin -->

@endsection