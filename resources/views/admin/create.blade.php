@extends('layouts.admin')

@section('content')

<div id="include-watch" class="row">

    <div class="bg-img col-sm-12">

        <div class="row">

            <div id="form-box"  class="col-sm-8 col-sm-offset-2">

                <div id="panel-heading" class="panel-heading">Include a New Watch</div>

                <form
                class="form-horizontal"
                    method="POST"
                    action="/admin/watches">
                    {{ csrf_field() }}

                    <div class="row field-form">

                        <!-- Title -->
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-sm-4 control-label">Title</label>
                            <div class="col-sm-8">
                                <input id="title" type="number" class="form-control"
                                       name="title" value="{{ old('title') }}" required autofocus>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Title -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- Price -->
                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price" class="col-sm-4 control-label">Price</label>
                            <div class="col-sm-8">
                                <input id="price" type="number" class="form-control"
                                       name="price" value="{{ old('price') }}" required autofocus>
                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Price -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- Category -->
                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label for="category_id" class="col-sm-4 control-label">Category Id</label>
                            <div class="col-sm-8">
                                <input id="category_id" type="number" class="form-control"
                                       name="category_id" value="{{ old('category_id') }}" required autofocus>
                                @if ($errors->has('category_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Category -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- Weight -->
                        <div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
                            <label for="weight" class="col-sm-4 control-label">Weight</label>
                            <div class="col-sm-8">
                                <input id="weight" type="number" class="form-control"
                                       name="weight" value="{{ old('weight') }}" required autofocus>
                                @if ($errors->has('weight'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Weight -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- Length -->
                        <div class="form-group{{ $errors->has('length') ? ' has-error' : '' }}">
                            <label for="length" class="col-sm-4 control-label">Length</label>
                            <div class="col-sm-8">
                                <input id="length" type="number" class="form-control"
                                    name="length" value="{{ old('length') }}" required autofocus>
                                @if ($errors->has('length'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('length') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Length -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- Size -->
                        <div class="form-group{{ $errors->has('size') ? ' has-error' : '' }}">
                            <label for="size" class="col-sm-4 control-label">Size</label>
                            <div class="col-sm-8">
                                <input id="size" type="number" class="form-control"
                                       name="size" value="{{ old('size') }}" required autofocus>
                                @if ($errors->has('size'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('size') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Size -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- Sku -->
                        <div class="form-group{{ $errors->has('sku') ? ' has-error' : '' }}">
                            <label for="sku" class="col-sm-4 control-label">Sku</label>
                            <div class="col-sm-8">
                                <input id="sku" type="number" class="form-control"
                                       name="sku" value="{{ old('sku') }}" required autofocus>
                                @if ($errors->has('sku'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sku') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Sku -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- Company Cost -->
                        <div class="form-group{{ $errors->has('company_cost') ? ' has-error' : '' }}">
                            <label for="company_cost" class="col-sm-4 control-label">Company Cost</label>
                            <div class="col-sm-8">
                                <input id="company_cost" type="number" class="form-control"
                                       name="company_cost" value="{{ old('company_cost') }}" required autofocus>
                                @if ($errors->has('company_cost'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('company_cost') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Company Cost -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- Instock -->
                        <div class="form-group{{ $errors->has('instock') ? ' has-error' : '' }}">
                            <label for="instock" class="col-sm-4 control-label">Instock</label>
                            <div class="col-sm-8">
                                <input id="instock" type="number" class="form-control"
                                       name="instock" value="{{ old('instock') }}" required autofocus>
                                @if ($errors->has('instock'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('instock') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Instock -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- Shipping Cost -->
                        <div class="form-group{{ $errors->has('shipping_cost') ? ' has-error' : '' }}">
                            <label for="shipping_cost" class="col-sm-4 control-label">Shipping Cost</label>
                            <div class="col-sm-8">
                                <input id="shipping_cost" type="number" class="form-control"
                                       name="shipping_cost" value="{{ old('shipping_cost') }}" required autofocus>
                                @if ($errors->has('shipping_cost'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shipping_cost') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Shipping Cost -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- Image -->
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-sm-4 control-label">Image</label>
                            <div class="col-sm-8">
                                <input id="image" type="number" class="form-control"
                                       name="image" value="{{ old('image') }}" required autofocus>
                                @if ($errors->has('image'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Image -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- Short Description -->
                        <div class="form-group{{ $errors->has('short_description') ? ' has-error' : '' }}">
                            <label for="short_description" class="col-sm-4 control-label">Short Description</label>
                            <div class="col-sm-8">
                                <input id="short_description" type="number" class="form-control"
                                       name="short_description" value="{{ old('short_description') }}" required autofocus>
                                @if ($errors->has('short_description'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('short_description') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Short Description -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <!-- Long Description -->
                        <div class="form-group{{ $errors->has('long_description') ? ' has-error' : '' }}">
                            <label for="long_description" class="col-sm-4 control-label">Long Description</label>
                            <div class="col-sm-8">
                                <input id="long_description" type="number" class="form-control"
                                       name="long_description" value="{{ old('long_description') }}" required autofocus>
                                @if ($errors->has('long_description'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('long_description') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Long Description -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div><!-- /.form-group Register Button -->

                    </div><!-- /.row field-form -->

                </form>

            </div><!-- /.col-sm-8 col-sm-offset-2 -->

        </div><!-- /#form-box row -->

    </div><!-- /.background-img col-sm-12 -->

</div><!-- /#register .row -->
@endsection