@extends('layouts.admin')

@section('content')

<div id="edit-admin">

    <div class="row">

        <div class="watches_bar col-sm-12">

            <nav class="navbar navbar-inverse">
                <ul class="nav navbar-nav">
                    <li>Edit Tag</li>
                </ul>
            </nav>

        </div><!-- /.watches_bar .col-sm-12 -->

    </div><!-- /.row -->

    <div class="row">

        <div class="profile-box profile_sidebar col-sm-12">

            {!! Form::model($tag,['url'=>'/admin/tag', 'method'=>'PATCH']) !!}

            {{ csrf_field() }}

            <div class="profile_box-data">

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('tag_name', 'Name') !!}
                    {!! Form::text('tag_name') !!}
                    </div>
                </div>

            </div><!-- /.profile_box -->

            <!-- Update profile button -->
            <div class="form-group">
                {!! Form::hidden('id') !!}
                <button type="submit" class="btn btn-primary">Update Tag</button>
            </div>

            {!! Form::close() !!}

        </div><!-- /.profile-box profile_sidebar col-sm-9 -->

    </div><!-- /row -->

</div><!-- /#edit-admin -->
@endsection