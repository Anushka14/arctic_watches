@extends('layouts.admin')

@section('content')

<div id="include-watch" class="row">

    <div class="bg-img col-sm-12">

        <div class="row">

            <div id="form-box"  class="col-sm-8 col-sm-offset-2">

                <div id="panel-heading" class="panel-heading">Include a New Tag</div>

                <form
                    class="form-horizontal"
                    method="POST"
                    action="/admin/tag">
                    {{ csrf_field() }}

                    <div class="row field-form">

                        <!-- Title -->
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Tag Id</label>
                            <div class="col-sm-8">
                                <input id="title" type="number" class="form-control"
                                       name="title" value="{{ old('title') }}" required autofocus>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group Title -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <div class="form-group{{ $errors->has('tag_name') ? ' has-error' : '' }}">
                            <label for="tag_name" class="col-sm-4 control-label">Tag Name</label>
                            <div class="col-sm-8">
                                <input id="tag_name" type="text" class="form-control"
                                       name="tag_name" value="{{ old('tag_name') }}" required autofocus>
                                @if ($errors->has('tag_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tag_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div><!-- /.form-group Register Button -->

                    </div><!-- /.row field-form -->

                </form>

            </div><!-- /.col-sm-8 col-sm-offset-2 -->

        </div><!-- /#form-box row -->

    </div><!-- /.background-img col-sm-12 -->

</div><!-- /#register .row -->
@endsection