@extends('layouts.admin')

@section('content')

    <div>
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ URL::to('admin/index') }}">Tags</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('admin/tag') }}">View All Tags</a></li>
                <li><a href="{{ URL::to('admin/tag/create') }}">Create a Tag</a>
            </ul>
        </nav>

        <h1>All the Watches</h1>

        <!-- will be used to show any messages -->
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif


        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>

            </tr>
            </thead>
            <tbody>
            @foreach($tag as $key => $watch_tag)
                <tr>
                    <td>{{ $watch_tag->id }}</td>
                    <td>{{ $watch_tag->tag_name }}</td>

                    <!-- we will also add show, edit, and delete buttons -->
                    <td>
                        <a class="btn btn-small btn-info"
             href="/admin/tag/{{ $watch_tag->id }}/edit">Edit this Tag</a>
                        <a class="btn btn-small btn-danger" href="/admin/tag/{{ $watch_tag->id }}">
                            Delete this Tags
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>




    </div>
    </body>
    </html>

@endsection