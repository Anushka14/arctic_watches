@extends('layouts.admin')

@section('content')

<div id="watches" class="row">

    <div class="watches_bar col-sm-12">

        <nav class="navbar navbar-inverse">
            <!-- <div class="navbar-header">
                <a class="navbar-brand" href="{{ URL::to('admin/index') }}">Watches</a>
            </div> -->
            <ul class="nav navbar-nav">
                <!-- <li><a href="{{ URL::to('admin/index') }}">View All Watches</a></li> -->
                <li><a href="{{ URL::to('admin/watches/create') }}">Create a Watch</a>
            </ul>
        </nav>

    </div><!-- /.col-sm-12 -->

        <h1>All the Watches</h1>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Price</td>
                <td>Category_id</td>
                <td>Weight</td>
                <td>Length</td>
                <td>Sku</td>
                <td>company_cost</td>
                <td>instock</td>
                <td>shipping_cost</td>
                <td>image</td>
                <td>short_description</td>
                <td>long_description</td>
                <td>Actions</td>
            </tr>
        </thead>
        <tbody>
        @foreach($watches as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->title }}</td>
                <td>{{ $value->price }}</td>
                <td>{{ $value->category_id }}</td>
                <td>{{ $value->weight }}</td>
                <td>{{ $value->length }}</td>
                <td>{{ $value->sku }}</td>
                <td>{{ $value->company_cost }}</td>
                <td>{{ $value->instock }}</td>
                <td>{{ $value->shipping_cost }}</td>
                <td>{{ $value->image }}</td>
                <td>{{ $value->short_description }}</td>
                <td>{{ $value->long_description }}</td>
                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    <a class="btn btn-small btn-info" href="{{ URL::to('admin/watches/' . $value->id .
                     '/edit') }}">Edit this Watch</a>
                    <a class="btn btn-small btn-danger" href="/admin/watches/{{ $value->id }}/admin/watches">
                        Delete this Category
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{$watches->links()}}

</div><!-- /#watches -->

@endsection