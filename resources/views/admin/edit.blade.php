@extends('layouts.admin')

@section('content')

<div id="edit-admin">

    <div class="row">

        <div class="watches_bar col-sm-12">

            <nav class="navbar navbar-inverse">
                <ul class="nav navbar-nav">
                    <li>Edit Watch</li>
                </ul>
            </nav>

        </div><!-- /.watches_bar .col-sm-12 -->

    </div><!-- /.row -->

    <div class="row">

        <div class="profile-box profile_sidebar col-sm-12">

            {!! Form::model($watch,['url'=>'/admin/watches', 'method'=>'PATCH']) !!}

            {{ csrf_field() }}

            <div class="profile_box-data">

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('name', 'Title') !!}
                    {!! Form::text('title') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('price', 'Price') !!}
                    {!! Form::text('price') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('category_id', 'Category') !!}
                    {!! Form::text('category_id') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('weight', 'Weight') !!}
                    {!! Form::text('weight') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('length', 'Length') !!}
                    {!! Form::text('length') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('sku', 'sku') !!}
                    {!! Form::text('sku') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('company_cost', 'company_cost') !!}
                    {!! Form::text('company_cost') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('instock', 'instock') !!}
                    {!! Form::text('instock') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('shipping_cost', 'shipping_cost') !!}
                    {!! Form::text('shipping_cost') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('image', 'image') !!}
                    {!! Form::text('image') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('short_description', 'short_description') !!}
                    {!! Form::text('short_description') !!}
                    </div>
                </div>

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                    {!! Form::label('long_description', 'long_description') !!}
                    {!! Form::text('long_description') !!}
                    </div>
                </div>

            </div><!-- /.profile_box -->

            <!-- Update profile button -->
            <div class="form-group">
                {!! Form::hidden('id') !!}
                <button type="submit" class="btn btn-primary">Update Watch</button>
            </div>

            {!! Form::close() !!}

        </div><!-- /.profile-box profile_sidebar col-sm-9 -->

    </div><!-- /row -->

</div><!-- /#edit-admin -->
@endsection