@extends('layouts.admin')

@section('content')

<div id="edit-admin">

    <div class="row">

        <div class="watches_bar col-sm-12">

            <nav class="navbar navbar-inverse">
                <ul class="nav navbar-nav">
                    <li>Edit Category</li>
                </ul>
            </nav>

        </div><!-- /.watches_bar .col-sm-12 -->

    </div><!-- /.row -->

    <div class="row">

        <div class="profile-box profile_sidebar col-sm-12">

            {!! Form::model($category,['url'=>'/admin/category', 'method'=>'PATCH']) !!}

            {{ csrf_field() }}

            <div class="profile_box-data">

                <div class="edit-profile_detail">
                    <div class="edit_profile_form">
                        {!! Form::label('category_name', 'Name') !!}
                        {!! Form::text('category_name') !!}
                    </div>
                </div>

            </div><!-- /.profile_box -->

            <!-- Update profile button -->
            <div class="form-group">
                {!! Form::hidden('id') !!}
                <button type="submit" class="btn btn-primary">Update Category</button>
            </div>

            {!! Form::close() !!}

        </div><!-- /.profile-box profile_sidebar col-sm-9 -->

    </div><!-- /row -->

</div><!-- /#edit-admin -->
@endsection