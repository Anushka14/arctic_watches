[1:00 AM, 12/6/2017] Vaneesa: 
[1:00 AM, 12/6/2017] Vaneesa: @extends('layouts.admin')

@section('content')

<div id="include-watch" class="row">

    <div class="bg-img col-sm-12">

        <div class="row">

            <div id="form-box"  class="col-sm-8 col-sm-offset-2">

                <div id="panel-heading" class="panel-heading">Include a New Category</div>

                <form
                class="form-horizontal"
                    method="POST"
                    action="/admin/category">
                    {{ csrf_field() }}

                    <div class="row field-form">

                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label for="category_id" class="col-sm-4 control-label">Category Id</label>
                            <div class="col-sm-8">
                                <input id="category_id" type="number"  class="form-control"
                                       name="category_id" value="{{ old('category_id') }}" required autofocus>
                                @if ($errors->has('category_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <div class="form-group{{ $errors->has('category_name') ? ' has-error' : '' }}">
                            <label for="category_name" class="col-sm-4 control-label">Category Name</label>
                            <div class="col-sm-8">
                                <input id="category_name" type="text"  class="form-control"
                                       name="category_name" value="{{ old('category_name') }}" required autofocus>
                                @if ($errors->has('category_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><!-- /.form-group -->

                    </div><!-- /.row field-form -->

                    <div class="row field-form">

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div><!-- /.form-group Register Button -->

                    </div><!-- /.row field-form -->

                </form>

            </div><!-- /.col-sm-8 col-sm-offset-2 -->

        </div><!-- /#form-box row -->

    </div><!-- /.background-img col-sm-12 -->

</div><!-- /#register .row -->
@endsection