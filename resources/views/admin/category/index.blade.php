@extends('layouts.admin')

@section('content')

<div id="watches" class="row">

    <div class="watches_bar col-sm-12">

        <nav class="navbar navbar-inverse">
            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('/admin/category/create') }}">Create a Category</a>
            </ul>
        </nav>

    </div><!-- /.col-sm-12 -->

        <h1>All the Watches</h1>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
            </tr>
        </thead>
        <tbody>
            @foreach($category as $key => $watch_category)
                <tr>
                    <td>{{ $watch_category->id }}</td>
                    <td>{{ $watch_category->category_name }}</td>

                    <td>
                        {{ Form::open(array('url' => 'admin/category/' . $watch_category->id, 'class' => 'pull-right')) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::submit('Delete this Category', array('class' => 'btn btn-warning')) }}
                        {{ Form::close() }}
                        <a class="btn btn-small btn-info" href="/admin/category/{{ $watch_category->id }}/edit">
                            Edit this Category
                        </a>
                        {{--<a class="btn btn-small btn-danger" href="/admin/category/{{ $watch_category->id }}/edit">--}}
                            {{--Delete this Category--}}
                        {{--</a>--}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

</div><!-- /#watches -->

@endsection