@extends('layouts.admin')

@section('content')

    <div id="edit-profile" class="row">

        <div class="bg-img col-sm-12">

            <div class="row">

                <div class="profile-box profile_sidebar col-sm-3">

                    <div id="panel-heading" class="panel-heading">
                        <h2>User Information</h2>
                    </div>

                    <div id="nav-sidebar-profile">
                        <ul>
                            <li><a href="/admin/admin_users/index">Users</a></li>
                            <li><a href="/admin/admin_users/create">Create a User</a></li>
                        </ul>
                    </div><!-- /.nav-sidebar-profile -->

                </div><!-- /profile-box profile_sidebar col-sm-4 -->

                <div class="profile-box profile_sidebar col-sm-9">

                    <div id="panel-heading" class="panel-heading">
                        <h2>Add A User</h2>
                    </div>

                    <div class="profile_box-data" id="edit-profile-h4"><h4>Adjust Watches fields below</h4></div>

                    <form class="form-horizontal" method="POST" action="/admin_users/index">
                        {{ csrf_field() }}

                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                <label for="name" class="col-md-4 control-label">Name</label>

                                <div class="">
                                    <input id="name" type="text" class="form-control"
                                           name="title" value="{{ old('name') }}" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                              <strong>{{ $errors->first('name') }}</strong>
                                         </span>
                                    @endif
                                </div>
                            </div>
                        </div>



                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                <label for="email" class="col-md-4 control-label">Email</label>

                                <div class="">
                                    <input id="email" type="email" class="form-control"
                                           name="email" value="{{ old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                              <strong>{{ $errors->first('email') }}</strong>
                                         </span>
                                    @endif
                                </div>
                            </div>
                        </div>




                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                <label for="username" class="col-md-4 control-label">Username</label>

                                <div class="">
                                    <input id="username" type="number" class="form-control"
                                           name="username" value="{{ old('username') }}" required autofocus>
                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                              <strong>{{ $errors->first('username') }}</strong>
                                         </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                <label for="street" class="col-md-4 control-label">Street</label>

                                <div class="">
                                    <input id="street" type="text" class="form-control"
                                           name="street" value="{{ old('street') }}" required autofocus>
                                    @if ($errors->has('street'))
                                        <span class="help-block">
                                              <strong>{{ $errors->first('street') }}</strong>
                                         </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                <label for="postal" class="col-md-4 control-label">Postal</label>

                                <div class="">
                                    <input id="postal" type="text" class="form-control"
                                           name="postal" value="{{ old('postal') }}" required autofocus>
                                    @if ($errors->has('postal'))
                                        <span class="help-block">
                                              <strong>{{ $errors->first('postal') }}</strong>
                                         </span>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                <label for="city" class="col-md-4 control-label">City</label>

                                <div class="">
                                    <input id="city" type="text" class="form-control"
                                           name="city" value="{{ old('city') }}" required autofocus>
                                    @if ($errors->has('city'))
                                        <span class="help-block">
                                              <strong>{{ $errors->first('city') }}</strong>
                                         </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                <label for="province" class="col-md-4 control-label">Province</label>

                                <div class="">
                                    <input id="province" type="text" class="form-control"
                                           name="province" value="{{ old('province') }}" required autofocus>
                                    @if ($errors->has('province'))
                                        <span class="help-block">
                                              <strong>{{ $errors->first('province') }}</strong>
                                         </span>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                <label for="country" class="col-md-4 control-label">Country</label>

                                <div class="">
                                    <input id="country" type="text" class="form-control"
                                           name="country" value="{{ old('country') }}" required autofocus>
                                    @if ($errors->has('country'))
                                        <span class="help-block">
                                              <strong>{{ $errors->first('country') }}</strong>
                                         </span>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="">
                                    <input id="password" type="text" class="form-control"
                                           name="password" value="{{ old('password') }}" required autofocus>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                              <strong>{{ $errors->first('password') }}</strong>
                                         </span>
                                    @endif
                                </div>
                            </div>
                        </div>








                        <div>
                            <div>
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>

                </div><!-- /.profile-box profile_sidebar col-sm-9 -->

            </div><!-- /#form-box row -->

        </div><!-- /.background-img col-sm-12 -->

    </div><!-- /#register .row -->
@endsection
