@extends('layouts.admin')

@section('content')

    <div id="edit-admin" class="row">

        <div class="bg-img col-sm-12">

            <div class="row">

                <div class="profile-box profile_sidebar col-sm-3">

                    <div id="panel-heading" class="panel-heading">
                        <h2>User Information</h2>
                    </div>

                    <div id="nav-sidebar-profile">
                        <ul>
                            <li><a href="/admin/admin_users/index" >Users</a></li>
                            <li class="active_profile"><a href="/admin/admin_users/create">Create a User</a></li>

                        </ul>
                    </div><!-- /.nav-sidebar-profile -->

                </div><!-- /profile-box profile_sidebar col-sm-4 -->

                <div class="profile-box profile_sidebar col-sm-9">

                    <div id="panel-heading" class="panel-heading">
                        <h2>Edit Users</h2>
                    </div>

                    <div id="edit-admin-h4"><h4>Adjust User fields below</h4></div>

                    {!! Form::model($users,['url'=>'/admin/admin_users/index', 'method'=>'PATCH']) !!}

                    {{ csrf_field() }}

                    <div class="profile_box-data">

                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                {!! Form::label('name', 'Full Name') !!}
                                {!! Form::text('name') !!}
                            </div>
                        </div>

                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                {!! Form::label('username', 'Username') !!}
                                {!! Form::text('username') !!}
                            </div>
                        </div>

                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                {!! Form::label('email', 'Email') !!}
                                {!! Form::text('email') !!}
                            </div>
                        </div>

                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                {!! Form::label('street', 'Street') !!}
                                {!! Form::text('street') !!}
                            </div>
                        </div>

                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                {!! Form::label('postal', 'Postal Code') !!}
                                {!! Form::text('postal') !!}
                            </div>
                        </div>

                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                {!! Form::label('city', 'City') !!}
                                {!! Form::text('city') !!}
                            </div>
                        </div>

                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                {!! Form::label('province', 'Province') !!}
                                {!! Form::text('province') !!}
                            </div>
                        </div>

                        <div class="edit-profile_detail">
                            <div class="edit_profile_form">
                                {!! Form::label('country', 'Country') !!}
                                {!! Form::text('country') !!}
                            </div>
                        </div>



                    </div><!-- /.profile_box -->

                    <!-- Update profile button -->
                    <div class="button">
                        {!! Form::hidden('id') !!}
                        <button type="submit" id="button" class="btn btn-primary">Update User</button>
                    </div>

                    {!! Form::close() !!}

                </div><!-- /.profile-box profile_sidebar col-sm-9 -->

            </div><!-- /#form-box row -->

        </div><!-- /.background-img col-sm-12 -->

    </div><!-- /#register .row -->
@endsection
