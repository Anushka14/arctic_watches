@extends('layouts.admin')

@section('content')
    <div>
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ URL::to('admin/admin_users/index') }}">Users</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('admin/admin_users/index') }}">View All Users</a></li>
                <li><a href="{{ URL::to('admin/admin_users/create') }}">Create a User</a>
            </ul>
        </nav>

        <h1>All the Users</h1>

        <!-- will be used to show any messages -->
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif

            <table class="table table-striped table-bordered">
                <tr>
                    <td><strong>ID</strong></td>
                    <td><strong>Name</strong></td>
                    <td><strong>Username</strong></td>
                    <td><strong>Email</strong></td>
                    <td><strong>Street</strong></td>
                    <td><strong>Postal Code</strong></td>
                    <td><strong>City</strong></td>
                    <td><strong>Province</strong></td>
                    <td><strong>Country</strong></td>
                    <td><strong>Actions</strong></td>
                </tr>
               
                @foreach($users as $user) 
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->street }}</td>
                    <td>{{ $user->postal }}</td>
                    <td>{{ $user->city }}</td>
                    <td>{{ $user->province }}</td>
                    <td>{{ $user->country }}</td>

                    <!-- we will also add show, edit, and delete buttons -->
                    <td>

                    <!-- delete the user will go here (use the destroy method DESTROY /users/{id} -->
                    {{ Form::open(array('url' => 'admin_users/' . $user->id, 'class' => 'pull-right')) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::submit('Delete User', array('class' => 'btn btn-warning')) }}
                    {{ Form::close() }}
                        <!-- edit this user (use the edit method found at GET /users/{id}/edit -->
                        <a class="btn btn-small btn-info" href="{{ URL::to('admin/admin_users/' . $user->id . '/edit') }}">Edit User</a>

                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
     
@endsection
