@extends('layouts.app')

@section('content')

<div class="row"><!-- section1 -->

	<div class="padding-zero col-sm-12">

		<div class="section1-male">

			<img src="images/male_texture2.jpg" alt="Women Texture">

			<div class="text_overlay">
					<div class="content">
						<div class="table">
							<div class="cell">
								<div class="inner">
									<h2>Gentlemen</h2>
									<h3>Feel the Power</h3>
								</div><!-- /.inner -->
							</div><!-- /.cell -->
						</div><!-- /.table -->
					</div><!-- /.content -->
				</div><!-- /.text_overlay -->

		</div><!-- /.section1 -->

	</div><!-- /.col-sm-12 -->

</div><!-- /.row -->

<div id="dropdown-women"><!-- multiCarousel1 -->

	<div class="row"><!-- carousel1 -->

		<div class="padding-zero col-sm-2"></div>

		<div class="padding-zero col-sm-8">

			<div>
				<h3>Arctic Men Trending</h3>
			</div>

			<div id="MultiCarousel1" class="MultiCarousel" data-items="2,3,3,4" data-slide="1" data-interval="1000">

				<div class="MultiCarousel-inner">

					@foreach($category->watches as $watch)
						<div class="item">
							<div class="pad15">
								<a href="/watch_detail/{{$watch->id}}">
									<img src="/images/Images/{{$watch->image}}"/>
								</a>
								<h2>{{$watch->title}}</h2>
							</div>
						</div><!-- /.item -->
					@endforeach

				</div><!-- /.MultiCarousel-inner -->

				<button class="btn btn-primary leftLst" type="button"><i class="fa fa-backward" aria-hidden="true"></i></button>
				<button class="btn btn-primary rightLst" type="button"><i class="fa fa-forward" aria-hidden="true"></i></button>

			</div><!-- /#MultiCarousel -->

		</div><!-- /.col-sm-8 -->

	 	<div class="padding-zero col-sm-2"></div>

	</div><!-- /.row carousel1  -->

</div><!-- /#dropdown-women -->


<div class="row"><!-- carousel2 -->

	<div class="padding-zero col-sm-2"></div>

	<div class="padding-zero col-sm-8">

		<div>
			<h3>Classic Men Collection</h3>
		</div>

		<div id="MultiCarousel2" class="MultiCarousel" data-items="2,3,3,4" data-slide="1" data-interval="1000">

			<div class="MultiCarousel-inner">

				@foreach($sports->watches as $sport_watch)
					<div class="item">
						<div class="pad15">
							<a href="/watch_detail/{{$sport_watch->id}}">
								<img src="images/Images/{{ $sport_watch->image }}" alt="watch" />
							</a>
							<h2>{{ $sport_watch->brand }}</h2>
						</div>
					</div><!-- /.item -->
				@endforeach

			</div><!-- /.MultiCarousel-inner -->

			<button class="btn btn-primary leftLst" type="button"><i class="fa fa-backward" aria-hidden="true"></i></button>
			<button class="btn btn-primary rightLst" type="button"><i class="fa fa-forward" aria-hidden="true"></i></button>

		</div><!-- /#MultiCarousel -->

	</div><!-- /.col-sm-8 -->

 	<div class="padding-zero col-sm-2"></div>

</div><!-- /.row carousel2  -->

<div class="center-block-female row"><!-- articles -->

	<div class="section padding-zero col-sm-6">
		<img src="images/male_section2.jpg" alt="watch" />
	</div><!-- /.col-xs-6 -->

	<div class="padding-zero col-sm-6">

		    <h3>Adventure</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum iaculis dui bibendum odio ullamcorper, vitae condimentum quam varius. Aliquam eleifend egestas sem ut placerat. Nunc vitae ex mollis, pretium ligula in, pretium neque. Praesent varius consequat sollicitudin. Aliquam sem justo, posuere sed blandit eget, lacinia eu mi. Ut non congue metus. Maecenas ultrices lacinia metus, vitae commodo arcu ornare ac. Sed elementum ipsum erat. Donec sit amet risus elit.</p>

	</div><!-- /.col-xs-6 -->

</div><!-- /.center-block row -->

<div class="center-block-female row"><!-- section1 -->

	<div class="padding-zero col-sm-6">

		    <h3>Aviator</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum iaculis dui bibendum odio ullamcorper, vitae condimentum quam varius. Aliquam eleifend egestas sem ut placerat. Nunc vitae ex mollis, pretium ligula in, pretium neque. Praesent varius consequat sollicitudin. Aliquam sem justo, posuere sed blandit eget, lacinia eu mi. Ut non congue metus. Maecenas ultrices lacinia metus, vitae commodo arcu ornare ac. Sed elementum ipsum erat. Donec sit amet risus elit.</p>

	</div><!-- /.col-xs-6 -->

	<div class="section padding-zero col-sm-6">
		<img src="images/male_section1.jpg" alt="watch" />
	</div><!-- /.col-xs-6 -->

</div><!-- /.center-block row -->

@endsection