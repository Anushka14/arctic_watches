@extends('layouts.admin')

@section('content')

<div id="dashboard" class="row">

    <div class="bg-img col-sm-12">

        <div class="row">

        	<div class="profile-box profile_sidebar col-sm-12">

                <div id="panel-heading" class="panel-heading">
                    <h2>Arctic Watches Admin Dashboard</h2>
                </div>

                <div class="dashboard_box row">
                	
                	<div class="dash_box col-sm-4">
                		<h2>Total Categories:</h2>
                		<h2>{{ $dashboard['categories'] }}</h2>
                	</div>

                	<div class="dash_box col-sm-4">
                		<h2>Total Watches:</h2>
                		<h2>{{ $dashboard['watches'] }}</h2>
                	</div>

                	<div class="dash_box col-sm-4">
                		<h2>Total Customers:</h2>
                		<h2>{{ $dashboard['customers'] }}</h2>
                	</div>

                </div><!-- /.row -->

                <div class="dashboard_box row">
                	
                	<div class="dash_box col-sm-4">
                		<h2>Total Orders:</h2>
                		<h2>{{ $dashboard['orders'] }}</h2>
                	</div>

                	<div class="dash_box col-sm-4">
                		<h2>Sales This Month:</h2>
                		<h2>$ {{ $dashboard['month_sales'] }}</h2>
                	</div>

                	<div class="dash_box col-sm-4">
                		<h2>Sales This Year:</h2>
                		<h2>$ {{ $dashboard['year_sales'] }}</h2>
                	</div>

                </div><!-- /.row -->

        	</div><!-- /.profile-box profile_sidebar col-sm-12 -->

        </div><!-- /.row -->

    </div><!-- /.bg-img col-sm-12 -->

</div><!-- /#register .row -->



@endsection