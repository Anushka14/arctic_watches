@extends('layouts.app')

@section('content')

<div id="cart">

	<div id="shopp_title" class="row">

		<div class="col-sm-12 shopp_line">
			<h1>Shopping Cart</h1>
			<hr>
		</div>

	</div><!-- /#shopp_title row -->

	<div id="order_items">

		<?php foreach(Cart::content() as $row) :?>

		<div id="line_item" class="row">

			<div class="row hidden-xs hidden-sm hidden-md">
				<div class="col-sm-3"><h3>Image</h3></div>
				<div class="col-sm-2"><h3>Item</h3></div>
				<div class="col-sm-3"><h3>Quantity</h3></div>
				<div class="col-sm-2"><h3>Price</h3></div>
				<div class="col-sm-2"><h3>Total</h3></div>

			</div><!-- /.row hidden-xs hidden-sm hidden-md -->

			<form
				method="post"
				action="/update"
				id="update"
				novalidate>

				<div class="col-sm-3 cart-watch-detail">
					<img src="/images/Images/{{ $row->options['image'] }}"/>
				</div>

				<div class="col-sm-2"><h3><?php echo $row->name; ?></h3></div>

				<div class="col-sm-3">

					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					<input type="hidden" name="row_id" value="{{ $row->rowId }}" />
					<div id="item_qty">
						<input type="text" name="qty" size="5" value="<?php echo $row->qty; ?>">
					</div>
					
					<br>

					<div class="cart_button">
						<input class="btn btn-primary" name="update" type="submit" value="Update Qty" />
						<input class="btn btn-primary" name="remove" type="submit" value="Remove" />
					</div>

				</div>

				<div class="col-sm-2"><h3>$ <?= number_format($row->price,2)?></h3></div>

				<div class="col-sm-2"><h3>$ <?= number_format($row->subtotal,2)?></h3></div>

			</form>

			<hr>
		
		</div><!-- .row -->

		<?php endforeach; ?>

		<div class="price_item">

			<div class="row">

				<div class="col-sm-8"></div>
				<div class="col-sm-2">
					<div><h3>Subtotal: </h3></div>
				</div>
				<div class="col-sm-2"><h3>$ <?php echo Cart::subtotal(); ?></h3></div>

			</div><!-- .row -->

			<div class="row">

				<div class="col-sm-8"></div>
				<div class="col-sm-2">
					<div><h3>GST/PST: </h3></div>
				</div>
				<div class="col-sm-2"><h3>$ <?php echo Cart::tax(); ?></h3></div>

			</div><!-- .row -->

			<div class="row">

				<div class="col-sm-8"></div>
				<div class="col-sm-2">
					<div><h3>Total: </h3></div>
				</div>
				<div class="col-sm-2"><h3>$ <?php echo Cart::total(); ?></h3></div>

			</div><!-- .row -->

			<hr>

		</div><!-- /.price_item -->

		<div class="row">

			<div class="col-sm-8"></div>
			<div class="col-sm-2 cart_button">
				<a class="btn btn-primary" href="/">Continue Shopping</a>
			</div>
			<div class="col-sm-2 cart_button">
				<a class="btn btn-primary" href="/checkout">Checkout</a>
			</div>

		</div><!-- .row -->

	</div><!-- /.order_items -->

</div><!-- /#cart -->

@endsection