$(document).ready(function() {
 	$('li.active').removeClass('active');
 	$('a[href="' + location.pathname + '"]').closest('li').addClass('active');
 	if (location.pathname == '/login') {
 		$('#login-menu').closest('li').addClass('active');
 		$('#register-menu').closest('li').removeClass('active');
 	} else if (location.pathname == '/register') {
 		$('#login-menu').closest('li').removeClass('active');
 		$('#register-menu').closest('li').addClass('active');
 	} else {
 		$('#register-menu').closest('li').removeClass('active');
 		$('#login-menu').closest('li').removeClass('active');
 	}
});