<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Order;
use App\Watch;

class OrderWatch extends Model
{
    public function orders()
    {
    	return $this->belongsTo('App\Order');
    }

    public function watch()
    {
    	return $this->belongsTo('App\Watch');
    }
}

