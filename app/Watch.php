<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Category;
use App\Tag;
use App\OrderWatch;

class Watch extends Model
{

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function tags(){
        return $this->belongsToMany('App\Tag');
    }

    public function orderWatches(){
        return $this->belongsToMany('App\OrderWatch');
    }

    protected $fillable = [
        'id', 'title', 'price', 'category_id','weight','length','size',
        'sku','company_cost','instock','shipping_cost','image' ,'short_description',
        'long_description',
        ];
}

