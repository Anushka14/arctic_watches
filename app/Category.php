<?php

namespace App;

use App\Watch;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
     public function watches()
	 {
   		return $this->hasMany('App\Watch');
	 }

    protected $fillable = [
       'id', 'category_name',
    ];
}
