<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\OrderWatch;
use App\Transaction;

class Order extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function orderWatches()
    {
        return $this->hasMany('App\OrderWatch');
    }

    public function transaction()
    {
        return $this->hasOne('App\Transaction');
    }
   //ADDED FOR TEST
    protected $fillable = [
        'user_id', 'name', 'email', 'username','street','postal','province','city',
        'country','subtotal','taxes','total_price'
        ];
}
