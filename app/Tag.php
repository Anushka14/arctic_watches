<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Watch;

class Tag extends Model
{
    public function watches()
    {
    	return $this->belongsToMany(Watch::class);
    }

    protected $fillable = [
        'id', 'tag_name',
    ];
}
