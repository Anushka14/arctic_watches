<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Auth;
class AdminsController extends Controller
{
    public function isNotAdmin() {
        $user = Auth::user();
        if (!isset($user) || !$user->is_admin) return true;
        return false;
    }

    public function create()
    {

        // or user this line instead. Also at the begin of every routing function in every Admin controller
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        return view('admin');
    }
}
