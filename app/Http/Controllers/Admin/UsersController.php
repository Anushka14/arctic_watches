<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use \App\User;
use Session;
use \Auth;

class UsersController extends Controller
{
    /**
     * Get all Users
     * @return view
     */
	public function index()
	{
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $title = "Arctic Admin - All Users";
        $users = User::all();

        // load the view and pass the users

        return View('admin.users.index', compact('users','title'));
	}

    /**
     * Show the form for creating a new user
     * @return view
     */
    public function create()
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $title = 'Arctic Admin - Create User';

        // load the create user form (app/views/admin_users/create.blade.php)
        return View('admin.users.create', compact('title'));
    }

   /**
     * Display the specified user
     * @param  int  $id
     * @return view
     */
    public function show($id)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        // get the user by its ID
        $users = User::find($id);

        // show the view and pass the user to it
        return View('admin.users.show', compact($users));
    }

    /**
     * Show the form for editing the specified user
     * @param  int  $id
     * @return view
     */
    public function edit(User $users)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $title = 'Arctic Admin - Edit User';
        return view('admin.users.edit', compact('title', 'users'));
    }

    /**
     * Update the specified user in storage
     * @param  int  $request
     * @return redirect
     */
    public function update(Request $request)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'username' => 'required',
            'street' => 'required',
            'postal' => 'required',
            'city' => 'required',
            'province' => 'required',
            'country' => 'required'
        ]);

        $user = User::find($request->input('id'));
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->username = $request->input('username');
        $user->street = $request->input('street');
        $user->postal = $request->input('postal');
        $user->city = $request->input('city');
        $user->province = $request->input('province');
        $user->country = $request->input('country');

        $user->update();
        Session::flash('message', 'User has been successfully edited');
        return redirect('/admin/users/index');
    }

    /**
     * Store a newly created user in storage
     * @param string $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'username' => 'required',
            'street' => 'required',
            'postal' => 'required',
            'city' => 'required',
            'province' => 'required',
            'country' => 'required',
            'password' =>'required'
        ]);

        $data = $request->all();

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'street' => $data['street'],
            'postal' => $data['postal'],
            'city' => $data['city'],
            'province' => $data['province'],
            'country' => $data['country'],
            'password' => $data['password']
        ]);
        Session::flash('message', 'User has been successfully created');
        return redirect('/admin/users/index');
    }    
   
    /**
     * Remove the specified user from storage
     * @param  int  $id
     * @return redirect
     */
    public function destroy($id)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $users = User::find($id);
        $users->delete();

        // redirect to index.blade with success msg
        Session::flash('message', 'User has been successfully deleted');
        return Redirect::to('/admin/users/index');
    }
} // end UsersController
