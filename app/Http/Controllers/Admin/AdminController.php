<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Auth;
use App\Category;
use App\Watch;
use App\Order;
use App\User;

class AdminController extends Controller
{
    public function dashboard()
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');

        $month_sales = 0;
        $year_sales = 0;

        $rows = DB::select('SELECT SUM(total_price) as total_sales FROM orders WHERE ' .
        		'MONTH(created_at) = MONTH(CURRENT_DATE()) AND YEAR(created_at) = YEAR(CURRENT_DATE())');
        foreach ($rows as $row) {
        	$month_sales = $row->total_sales;
        }

        $rows = DB::select('SELECT SUM(total_price) as total_sales FROM orders WHERE ' .
        		'YEAR(created_at) = YEAR(CURRENT_DATE())');
        foreach ($rows as $row) {
        	$year_sales = $row->total_sales;
        }

        $dashboard = [
        	"categories"=>Category::count(),
        	"watches"=>Watch::count(),
        	"customers"=>User::count(),
        	"orders"=>Order::count(),
        	"month_sales"=>$month_sales,
        	"year_sales"=>$year_sales
        ];

        return view('dashboard', compact( 'dashboard'));
    }

    public function index()
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $category= Category::all();
        return view('admin.category.index', compact( 'category'));
    }
}
