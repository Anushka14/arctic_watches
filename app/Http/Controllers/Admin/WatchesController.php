<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Watch;
use \Auth;

class WatchesController extends \App\Http\Controllers\Controller
{
    public function create()
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        return view('admin.create');
    }

    protected function validator(Request $request)
    {
        $data = $request->all();

        return Validator::make($data, [
            'id' => 'required',
            'title' => 'required',
            'price' => 'required',
            'category_id' => 'required',
            'weight' => 'required',
            'length' => 'required',
            'sku' => 'required',
            'company_cost' => 'required',
            'instock' => 'required',
            'shipping_cost' => 'required',
            'image' => 'required',
            'short_description' => 'required',
            'long_description' => 'required',
        ]);
    }

    public function index()
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $title = 'Watches';
        $watches = DB::table('watches')->paginate(15);
        return view('admin.index', compact('title', 'watches'));
    }

    public function edit(Watch $watch)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $title = 'Edit Watches';
        return view('admin.edit', compact('title', 'watch'));
    }


    public function update(Request $request)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $this->validate($request, [
            'id' => 'required',
            'title' => 'required',
            'price' => 'required',
            'category_id' => 'required',
            'weight' => 'required',
            'length' => 'required',
            'sku' => 'required',
            'company_cost' => 'required',
            'instock' => 'required',
            'shipping_cost' => 'required',
            'image' => 'required',
            'short_description' => 'required',
            'long_description' => 'required',
        ]);
        
        $watch = Watch::find($request->input('id'));
        $watch->title = $request->input('title');
        $watch->price = $request->input('price');
        $watch->category_id = $request->input('category_id');
        $watch->weight = $request->input('weight');
        $watch->length = $request->input('length');
        $watch->sku = $request->input('sku');
        $watch->company_cost = $request->input('company_cost');
        $watch->instock = $request->input('instock');
        $watch->shipping_cost = $request->input('shipping_cost');
        $watch->image = $request->input('image');
        $watch->short_description = $request->input('short_description');
        $watch->long_description = $request->input('long_description');

        $watch->update();
        return redirect('/admin/watches');
    }

    public function store(Request $request)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $this->validate($request, [
            'title' => 'required',
            'price' => 'required',
            'category_id' => 'required',
            'weight' => 'required',
            'length' => 'required',
            'sku' => 'required',
            'company_cost' => 'required',
            'instock' => 'required',
            'shipping_cost' => 'required',
            'image' => 'required',
            'short_description' => 'required',
            'long_description' => 'required',
        ]);

        $data = $request->all();

        Watch::create([
            'title' => $data['title'],
            'price' => $data['price'],
            'category_id' => $data['category_id'],
            'weight' => $data['weight'],
            'length' => $data['length'],
            'size' => $data['size'],
            'sku' => $data['sku'],
            'company_cost' => $data['company_cost'],
            'instock' => $data['instock'],
            'shipping_cost' => $data['shipping_cost'],
            'image' => $data['image'],
            'short_description' => $data['short_description'],
            'long_description' => $data['long_description'],
        ]);
        return redirect('/admin/watches');
    }

    public function destroy($id)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $title = 'Watches delete Page';
        $watches= Watch::find($id);
        $watches->delete();
        return redirect('admin/watches');
    }

}