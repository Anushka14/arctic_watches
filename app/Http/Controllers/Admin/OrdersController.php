<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Order;
use \Auth;

class OrdersController extends Controller
{
    public function create()
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        //echo('<script>alert("inside create");</script>');
        return view('admin.orders.create');
    }

    protected function validator(Request $request)
    {
        $data = $request->all();

        return Validator::make($data, [
            'id' => 'required',
            'user_id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'username' => 'required',
            'street' => 'required',
            'postal' => 'required',
            'city' => 'required',
            'province' => 'required',
            'country' => 'required',
            'subtotal' => 'required',
            'taxes' => 'required',
            'total_price' => 'required',
            'transaction_status' => 'required',
        ]);
    }

    public function index()
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        //echo('<script>alert("from index func in controller");</script>');
        $title = 'Orders';
        $orders = DB::table('orders')->paginate(15);
        return view('admin.orders.index', compact('title', 'orders'));
    }

    public function edit(Order $order)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
       // echo('<script>alert("from edit func in controller");</script>');
        $title = 'Edit Orders';
        return view('admin.orders.edit', compact('title', 'order'));
    }


    public function update(Request $request)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        //echo('<script>alert("from update func in controller");</script>');

        $this->validate($request, [
            'id' => 'required',
            'user_id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'username' => 'required',
            'street' => 'required',
            'postal' => 'required',
            'city' => 'required',
            'province' => 'required',
            'country' => 'required',
            'subtotal' => 'required',
            'taxes' => 'required',
            'total_price' => 'required',
           
        ]);

        $order = Order::find($request->input('id'));
        $order->user_id = $request->input('user_id');
        $order->name = $request->input('name');
        $order->email = $request->input('email');
        $order->username = $request->input('username');
        $order->street = $request->input('street');
        $order->postal = $request->input('postal');
        $order->city = $request->input('city');
        $order->province = $request->input('province');
        $order->country = $request->input('country');
        $order->subtotal = $request->input('subtotal');
        $order->taxes = $request->input('taxes');
        $order->total_price = $request->input('total_price');
        $order->update();

        return redirect('/admin/orders');
    }

    public function store(Request $request)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        //echo('<script>alert("from store func in controller");</script>');
        $this->validate($request, [
            'user_id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'username' => 'required',
            'street' => 'required',
            'postal' => 'required',
            'city' => 'required',
            'province' => 'required',
            'country' => 'required',
            'subtotal' => 'required',
            'taxes' => 'required',
            'total_price' => 'required',
        ]);
        
        // echo('<script>alert("after creating order")</script>');
        $data = $request->all();

        Order::create([
            'user_id' => $data['user_id'],
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'street' => $data['street'],
            'postal' => $data['postal'],
            'city' => $data['city'],
            'province' => $data['province'],
            'country' => $data['country'],
            'subtotal' => $data['subtotal'],
            'taxes' => $data['taxes'],
            'total_price' => $data['total_price'],
        
        ]);
        return redirect('/admin/orders');
    }

    public function destroy($id)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $title = 'Orders delete Page';
        $orders= Order::find($id);
        $orders->delete();
        return redirect('admin/orders');
    }
}
