<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use \Auth;

class CategoriesController extends Controller
{
    public function index()
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $category= Category::all();
        return view('admin.category.index', compact( 'category'));
    }

    public function edit(Category $category)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $title= 'Edit Category';
        return view('admin.category.edit', compact('title','category'));
    }

    public function update(Request $request)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $this->validate($request, [
            'category_name' => 'required'
        ]);

        $category = Category::find($request->input('id'));
        $category->category_name = $request->input('category_name');
        $category->update();
        return redirect('/admin/category');
    }

    public function create()
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        return view('admin.category.create');
    }

    public function store(Request $request)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $this->validate($request, [
            'category_id' => 'required',
            'category_name' => 'required',
        ]);

        $data = $request->all();

        Category::create([
            'category_id' => $data['category_id'],
            'category_name' => $data['category_name'],

        ]);
        return redirect('/admin/category');

    }

    public function destroy($id)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $title = 'Category delete Page';
        $category= Category::find($id);
        $category->delete();
        return redirect('admin/category');
    }

}
