<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;
use \Auth;

class TagsController extends Controller
{
    public function index()
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $tag= Tag::all();
        return view('admin.tag.index', compact( 'tag'));
    }

    public function edit(Tag $tag)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $title= 'Edit Tag';
        return view('admin.tag.edit', compact('title','tag'));
    }

    public function update(Request $request)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $this->validate($request, [
            'tag_name' => 'required'
        ]);

        $tag = Tag::find($request->input('id'));
        $tag->tag_name = $request->input('tag_name');
        $tag->update();
        return redirect('/admin/tag');
    }

    public function create()
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        return view('admin.tag.create');
    }

    public function store(Request $request)
    {
        $user = Auth::user(); if (!isset($user) || !$user->is_admin) return redirect('/');
        $this->validate($request, [
            'tag_name' => 'required',
        ]);

        $data = $request->all();

        Tag::create([

            'tag_name' => $data['tag_name'],

        ]);

        return redirect('/admin/tag');

    }
}
