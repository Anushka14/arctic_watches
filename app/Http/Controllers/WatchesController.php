<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Watch;

use App\Category;

use App\Tag;

class WatchesController extends Controller
{


      public function index() {
          $title = 'Arctic Home Page';
          $trending = Tag::where('tag_name', 'Trending')->first();
          $sports = Tag::where('tag_name', 'Sports')->first();
          return view('home', compact('title', 'trending','sports'));
      }

     
    public function hers() {
        $title = 'Arctic Her Time';
        $category= Category::where('id', '1')->first();
        $trending = Tag::where('tag_name', 'Trending')->first();
        return view('hers', compact('title', 'category','trending'));
    }

    public function his() {
        $title = 'Arctic His Time';
        $category= Category::where('id', '2')->first();
        $sports = Tag::where('tag_name', 'Sports')->first();
        return view('his', compact('title', 'category','sports'));
    }

    public function watch_detail($id) {
        $title = 'Arctic Details';
        $watch=\DB::table('watches')->find($id);
        return view('watch_detail', compact('title','watch'));
    }

    public function about() {
        $title = 'Arctic Details';
        return view('about', compact('title'));
    }

    public function search(Request $request) {
        $term=$request->input('term');
        $title='search';
        $results=Watch::where('title','like','%'.$term.'%')->get();
        return view('search',compact('title','results'));
    }

    public function test(){
        $title='search';
        return view('search',compact('title'));
    }

}
