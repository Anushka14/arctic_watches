<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
  
    public function show()
    {
      $title = 'Profile';
      $user = Auth::user();
      return view('profile', compact('title', 'user'));
    }

    public function edit_profile() {
        $title = 'Edit Profile';
        $user = Auth::user();
        return view('edit_profile', compact('title', 'user'));
    } 

    public function update_profile(Request $request) {
      $this->validate($request, [
        'name' => 'required',
        'username' => 'required',
        'email' => 'required',
        'street' => 'required',
        'postal' => 'required',
        'city' => 'required',
        'province' => 'required',
        'country' => 'required'
      ]);
      
      $user = Auth::user();
      $user->name = $request->input('name');
      $user->username = $request->input('username');
      $user->email = $request->input('email');
      $user->street = $request->input('street');
      $user->postal = $request->input('postal');
      $user->city = $request->input('city');
      $user->province = $request->input('province');
      $user->country = $request->input('country');
      $user->update();
      return redirect('/profile');
    }  
  
    public function change_password(Request $request) {
      $title = 'Change Password';
      $user = Auth::user();
      return view('change_password', compact('title', 'user'));
    } 
  
    public function update_password(Request $request) {
      $this->validate($request, [
        'password' => 'required|confirmed'
      ]);
      
      $user = Auth::user();
      $user->password = bcrypt($request->input('password'));
      $user->update();
      return redirect('/change_password');
    }
}