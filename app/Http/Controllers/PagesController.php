<?php

namespace App\Http\Controllers;

use Session;

use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;

use App\User;
use App\Order;
use App\OrderWatch;

define('_5BX_API_LOGIN_ID', '5664341');

define('_5BX_API_KEY', '8DyBQ6G6ea0B6sSfG8hz8CLXEaQvIhoAT68OGkn4Gk00GH5V8fo9xBMyDmUK33LK');

class PagesController extends Controller
{
    public function cart()
    {
        $title = 'Arctic Shopping';
        return view('cart', compact('title'));
    }

    public function addToCart()
    {
        $title = 'Arctic Shopping Cart Add';
        Cart::add(['id'=> $_POST['id'], 'name'=>$_POST['name'],
            'qty'=>1, 'price'=>$_POST['price'], 'options'=>['image'=>$_POST['image'], 'watch_id'=>$_POST['id']] ]);
        return view('cart', compact('title'));
    }

    public function update()
    {
        $title = 'Arctic Shopping Cart Update';
        if (isset($_POST['update'])) {
            Cart::update($_POST['row_id'], $_POST['qty']);
        } else {
            if (isset($_POST['remove'])) {
                Cart::remove($_POST['row_id']);
            }
        }
        return view('cart', compact('title'));
    }

    public function checkout(Request $request)
    {
        $title = 'Arctic Checkout';
        if(Auth::user())
        {
            return view('checkout', compact('title'));
        }
        return redirect('/login');
    }

    public function confirm(Request $request)
    {
        $data = $request->all();
        $title = 'Arctic Confirm';

        $validator = $this->validator($data);

        if($validator->fails())
        {
            $title = 'Arctic Checkout';
            return redirect('/checkout')->withErrors($validator)->withInput();
        }

        $card = [];
        $card['card_type'] = $data['card_type'];
        $card['card_name'] = $data['card_name'];
        $card['card_number'] = $data['card_number'];
        $card['card_expire'] = $data['card_expire'];
        $card['card_cvv'] = $data['card_cvv'];

        $user = Auth::user();

        if(!isset($data['shipping_street']))
        {
            $data['shipping_street'] = $user['street'];
        }
        if(!isset($data['shipping_postal']))
        {
            $data['shipping_postal'] = $user['postal'];
        }
        if(!isset($data['shipping_city']))
        {
            $data['shipping_city'] = $user['city'];
        }
        if(!isset($data['shipping_province']))
        {
            $data['shipping_province'] = $user['province'];
        }
        if(!isset($data['shipping_country']))
        {
            $data['shipping_country'] = $user['country'];
        }
        
        $order = new Order;
        $order->user_id = Auth::id();
        $order->name = $user->name;
        $order->email = $user->email;
        $order->username = $user->username;
        $order->street = $user->street;
        $order->postal = $user->postal;
        $order->city = $user->city;
        $order->province = $user->province;
        $order->country = $user->country;
        $order->shipping_street = $data['shipping_street'];
        $order->shipping_postal = $data['shipping_postal'];
        $order->shipping_city = $data['shipping_city'];
        $order->shipping_province = $data['shipping_province'];
        $order->shipping_country = $data['shipping_country'];
        $order->subtotal = floatval(str_replace(',', '', Cart::subtotal()));
        $order->taxes = floatval(str_replace(',', '', Cart::tax()));
        $order->total_price = floatval(str_replace(',', '', Cart::total()));
        $order->transaction_status = 'pending';
        
        $order->save();

        $items = new Collection();

        foreach (Cart::content() as $item) {
            $orderWatch = new OrderWatch;
            $orderWatch->order_id = $order->id;
            $orderWatch->watch_id = $item->options['watch_id'];
            $orderWatch->qty = $item->qty;
            $orderWatch->name = $item->name;
            $orderWatch->unit_price = $item->price;
            $orderWatch->save();
            $items->push($orderWatch);
        }

        Cart::destroy();

        if($this->validateCreditCard($order, $card)) {
            return view('confirm', compact('title', 'order', 'items'));
        } else {
            Session::flash('error', 'There was a problem with your credit card information.');
            return back();
        }
        return view('confirm', compact('title', 'order', 'items'));
    }

    /**
     * Get a validator for Credit Card info.
     *
     * @param 
     * @return thankyou page
     */
    private function validateCreditCard($order, $card)
    {
        try {
            $gatewayTrans = new \App\_5bx;
            $gatewayTrans->amount($order->total_price);
            $gatewayTrans->card_num($card['card_number']); // credit card number
            $gatewayTrans->exp_date ($card['card_expire']); // expiry date month and year
            $gatewayTrans->cvv($card['card_cvv']); // card cvv number
            $gatewayTrans->ref_num($order->id); // your reference or invoice number
            $gatewayTrans->card_type($card['card_type']); // card type
            
            $response = $gatewayTrans->authorize_and_capture(); // returns JSON object
            // dd($response);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $trans = $response->transaction_response;
        // dd($trans);

        if($trans->response_code == 1){

            // transaction successful
            $order->transaction_status = 'Success';
            $order->save();
            // save transaction in transaction table with order->id
            $transaction = new \App\Transaction;
            $transaction->order_id = $order->id;
            $transaction->transaction = json_encode($trans);
            $transaction->save();
            // redirect to thankyou page
            return true;

        } else {

            // transaction fails
            // update order->transaction_status to fail
            $order->transaction_status = 'Fail';
            $order->save();
            // save transaction in transaction table with order->id
            $transaction = new \App\Transaction;
            $transaction->order_id = $order->id;
            $transaction->transaction = json_encode($trans);
            $transaction->save();
            // redirect back to payment page with flash error message
            return false;
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'card_type' => ['required', 'string',
                Rule::in(['Visa', 'Mastercard','American Express','Diners Club','Discover','JCB'])],
            'card_name' => 'required|string|max:120',
            'card_number' => ['required', 'numeric',
            'regex:/^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/'],
            'card_expire' => ['required', 'string',
                'regex:/^(1[0-2]|0[1-9]|\d)(\d{2})$/'],
            'card_cvv' => ['required', 'numeric',
                'regex:/^[0-9]{3,4}$/']
        ]);
    }
}