<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(seed_users_table::class);
         $this->call(seed_watches_table::class);
         $this->call(seed_tags_table::class);
         $this->call(seed_tag_watch_table::class);
         $this->call(seed_categories_table::class);
    }
}
