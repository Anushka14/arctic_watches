<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class seed_watches_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Trending
         */
        DB::table('watches')->insert([
            'title' => 'Rolex',
            'price' => '6245.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77K354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'rolex_women_6.jpg',
            'short_description' => 'Silver-toned dial with Grande Tapisserie pattern, pink gold applied hour-markers and Royal Oak hands with luminescent coating.',
            'long_description' => 'Selfwinding watch with date display and centre seconds. 18-carat pink gold case, silvered dial, 18-carat pink gold bracelet. 18-carat pink gold case, glareproofed sapphire crystal and caseback, screw-locked crown. 18-carat pink gold bracelet with AP folding clasp.  Its architecture, manufacturing quality and innovative features make it singularly precise and reliable.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Tissot',
            'price' => '5567.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77H354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'tissot_men_4.jpg',
            'short_description' => 'A powerful combination of modern aesthetics and prestigious traditional complication, the new 2015 Royal Oak Perpetual Calendar is the latest chapter of this incredible journey which began 140 years ago.',
            'long_description' => 'Silver-toned dial with Grande Tapisserie pattern, white gold applied hour-markers and Royal Oak hands with luminescent coating, silver-toned inner bezel.Stainless steel bracelet with AP folding clasp. Stainless steel case, glareproofed sapphire crystal and caseback, screw-locked crown. Stainless steel case, glareproofed sapphire crystal and caseback, screw-locked crown.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Montblanc',
            'price' => '9467.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77R354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'montblanc_men_2.jpg',
            'short_description' => 'Stainless steel case, glareproofed sapphire crystal and caseback, stainless steel bezel, black ceramic screw-locked crown and pushpieces, sandblasted titanium pushpiece guards.',
            'long_description' => 'Black dial, white gold hands with luminescent coating. Black rubber strap with stainless steel pin buckle. Paramagnetic blue Parachrom hairspring. High-performance Paraflex shock absorbers. Screw-down, Twinlock double waterproofness system. Scratch-resistant sapphire, Cyclops lens over the date. Waterproof to 100 metres / 330 feet',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Swiss Army',
            'price' => '7745.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354GF58',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'nixon_1.jpg',
            'short_description' => 'The central sweep seconds hand allows an accurate reading of 1/8 second, while the two counters on the dial display the lapsed time in hours and minutes. As with all the components of the watch, aesthetic controls by the human eye guarantee impeccable beauty.',
            'long_description' => 'Selfwinding chronograph. Stainless steel case, glareproofed sapphire crystal, caseback engraved with the legend Swiss Made Limited Edition, screw-locked crown. Silver-toned dial with Grande Tapisserie pattern, blue counters and outer zone, 30 minutes counter with green, red and white indexes, white gold applied hour-markers and Royal Oak hands with luminescent coating. Limited edition of 500 pieces.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Nixon',
            'price' => '9975.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'rolex_women_6.jpg',
            'short_description' => 'A powerful combination of modern aesthetics and prestigious traditional complication, the new 2015 Royal Oak Perpetual Calendar is the latest chapter of this incredible journey which began 140 years ago. Stainless steel case, glareproofed sapphire crystal and caseback, screw-locked crown.',
            'long_description' => 'The new selfwinding calibre 5134 is based on its predecessor, calibre 2120, however it has been enlarged in accordance with the updated 41 mm case size. The highly finished 4.31 mm thick movement is fully visible through the glareproofed sapphire crystal caseback. Stainless steel bracelet with AP folding clasp. A superalloy, 904L is extremely resistant and highly polishable. It maintains its beauty even in the harshest environments.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Omega',
            'price' => '7645.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77B354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'omega_4.jpg',
            'short_description' => 'Brown dial with Grande Tapisserie pattern, pink gold applied hour-markers and Royal Oak hands with luminescent coating.',
            'long_description' => 'This year, the Royal Oak is revisited as Omega opts for a casual take on its benchmark iconic design. Its cool character is enhanced with a Grande Tapisserie dial pattern and a choice of dial colours that give an edge to the precious metal finishings. Glareproofed sapphire crystal, diamond-set bezel, water-resistant to 50 m.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => "Jaquet Droz",
            'price' => '1000.00',
            'category_id' => '1',
            'weight' => '110g',
            'length' => '200mm',
            'size' => '20',
            'sku' => '7904F6H3G1',
            'company_cost' => '3000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'jaquet_droz_1.jpg',
            'short_description' => 'The Womens Marine Star Diamond Watch is one of the most prestigious watches available in the world today',
            'long_description' => 'New Marine Star in stainless steel case with 23 diamonds individually hand set on rose gold-tone accented bezel and dial, white mother-of-pearl inner dial, sapphire glass, screw-back case, rose gold-tone and stainless steel bracelet with double-press deployant closure, and water resistance to 100 meters.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

     DB::table('watches')->insert([
            'title' => 'Patek Phillip',
            'price' => '7250.00',
            'category_id' => '1',
            'weight' => '120g',
            'length' => '195mm',
            'size' => '20',
            'sku' => '77A354GF38',
            'company_cost' => '2500',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'patek_women_8.jpg',
            'short_description' => 'Black onyx dial with 18-karat red gold appliques 18-karat gold appliques of cherub, chariot and butterfly body Butterfly wings in 18-karat red gold Chariot wheel in 18-karat red gold.',
            'long_description' => 'Black onyx dial with hand-engraved 18-karat red gold appliques. 18-karat red gold case. Hand-winding mechanical automaton movement with push-button triggering mechanism moving the butterfly wings and wheel of the chariot. Self-winding mechanical hours and minutes movement. Power reserve of 68 hours. Diameter 43 mm. Numerus Clausus of 28.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Omega',
            'price' => '8650.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '7653490399',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'omega_6.jpg',
            'short_description' => 'Encircled by diamonds, the ladies Seamaster Aqua Terra is the true definition of elegance. With a symmetrical 34 mm case, this 18K Sedna™ gold model features a white pearled mother-of-pearl dial with a date window at 6 oclock and 11 diamond indexes in 18K Sedna™ gold holders.',
            'long_description' => 'Encircled by diamonds, the ladies Seamaster Aqua Terra is the true definition of elegance. With a symmetrical 34 mm case, this 18K Sedna™ gold model features a white pearled mother-of-pearl dial with a date window at 6 oclock and 11 diamond indexes in 18K Sedna™ gold holders. The bezel is diamond-set and the watch is presented on an integrated 18K Sedna™ gold bracelet. ',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Audemars',
            'price' => '3645.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354GF39',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'audemars_women_10.jpg',
            'short_description' => 'Silver-toned or blue dial with Grande Tapisserie pattern, yellow gold applied hour-markers and Royal Oak hands with luminescent coating.',
            'long_description' => 'The glow of gold in the new Royal Oak Quartz 33 mm edition is accentuated by the glittering 40 brilliant-cut diamonds (0.73 carats) on the bezel, and the Grande Tapisserie pattern on the silver or the new blue dial. Additional signature codes are also in place: 18-carat yellow gold case, yellow gold applied hour-markers and Royal Oak hands with luminescent coating.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Michael Kors',
            'price' => '8856.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77I354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'michael_kors_women_2.jpg',
            'short_description' => 'The primacy of craftsmanship in polished stainless steel, with grey ruthenium-toned dial in Grande Tapisserie motif.',
            'long_description' => 'Ruthenium-toned dial with white gold applied hour-markers and Royal Oak hands with luminescent coating. Stainless steel case, glareproofed sapphire crystal and caseback. Stainless steel bracelet with AP folding clasp. Easylink rapid extension system that allows the wearer to easily increase the bracelet length by approximately 5 mm, for additional comfort in any circumstance.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Swiss Army',
            'price' => '8456.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77C354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'swiss_army_11.jpg',
            'short_description' => 'Quartz watch with date display. Stainless steel case, silvered dial, white strap. 40 brilliant-cut diamonds; approx 0.71 carat (bezel)',
            'long_description' => 'Grande Tapisserie pattern, white gold applied hour-markers and Royal Oak hands with luminescent coating. Hand-stitched large square scale crocodile strap with stainless steel AP folding clasp or stainless steel bracelet with AP folding clasp. Mechanical excellence combined with quartz brings superior accuracy.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Rolex',
            'price' => '9044.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77D354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'rolex_men_1.jpg',
            'short_description' => 'A watch with two faces, an encounter between contrasts or a watchmaking union of opposites, the Royal Oak Offshore Selfwinding Tourbillon Chronograph is an amazing blend of two worlds. Limited Edition of 50.',
            'long_description' => 'Forged carbon case, glareproofed sapphire crystal caseback with Royal Oak Offshore engraving, black ceramic bezel, crown and pushpieces, titanium pushpiece guards. Black dial with Méga Tapisserie pattern, black counters, gold applied hour-markers and gold Royal Oak hands with luminescent coating. Sapphire inner ring. Black rubber with titanium pin buckle. Powered by the natural movements of your wrist.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Bvlova',
            'price' => '8456.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77E354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'bulova_men_1.jpg',
            'short_description' => 'Hand-stitched large square scale white alligator strap with diamond-set 18-carat pink gold AP folding clasp. Mechanical excellence combined with quartz brings superior accuracy.',
            'long_description' => 'Silver-toned opaline dial with Grande Tapisserie pattern, diamonds applied hour-markers and pink gold Royal Oak hands with luminescent coating. Quartz watch with date display. 18-carat white gold case. 172 brilliant-cut diamonds, ~1.89 carat case, bezel, buckle. 18-carat pink gold case set with diamonds (except links), glareproofed sapphire crystal, diamond-set bezel.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Rolex',
            'price' => '7444.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77P354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'rolex_men_3.jpg',
            'short_description' => 'Black enamel dial, white enamel Roman numerals and hour-markers, white gold hands. Our hand-wound watches are celebrated for their reliability.',
            'long_description' => 'A fitting homage to a true pioneer, this is the first classic collection timepiece endowed with the groundbreaking Supersonnerie triple-patented striking complication, which produces unprecedented harmonics and clarity. A slimmer bezel and enamel dial with enamel Roman numerals accentuate the classic design. 950 platinum case, glareproofed sapphire crystal. Hand-stitched "large square scale" black alligator strap with 950 platinum AP folding clasp.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Breitling',
            'price' => '6345.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77M354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'breitling_men_4.jpg',
            'short_description' => 'Slate grey dial with Grande Tapisserie pattern, black counters, white gold applied hour-markers and Royal Oak hands with luminescent coating, black inner bezel',
            'long_description' => 'Crafted in hand-finished black ceramic, this Royal Oak Perpetual Calendar features day, date, month, astronomical moon, and week of the year. The leap year indication — pioneered in 1955 — is also featured on the Grande Tapisserie decorated dial. Black ceramic case, glareproofed sapphire crystal and caseback. Black ceramic bracelet with titanium AP folding clasp',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Audemars',
            'price' => '7345.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77F354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'audemars_women2.jpg',
            'short_description' => 'Quartz watch with date display. Stainless steel case, silvered dial, black strap. 40 brilliant-cut diamonds; approx 0.71 carat (bezel). Glareproofed sapphire crystal, diamond-set bezel.',
            'long_description' => 'Black dial with Grande Tapisserie pattern, white gold applied hour-markers and Royal Oak hands with luminescent coating. Mechanical excellence combined with quartz brings superior accuracy.  The dials now feature rectangular index hour markers and longer hands, as well as a Chromalight display with long-lasting luminescence. Hand-stitched large square scale black alligator strap with stainless steel AP folding clasp.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        /**
         * Sports
         */
        DB::table('watches')->insert([
            'title' => 'Breitling',
            'price' => '1453.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77G354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'breitling_men_2.jpg',
            'short_description' => 'A future-forward design featuring brilliant-cut diamonds that break over the dial, bezel and bracelet in an asymmetrical wave. Intriguingly random, the stones are uniquely integrated into the 18-carat pink gold surface.',
            'long_description' => 'Diamond-set 18-carat pink gold bracelet with AP folding clasp. Stainless steel case, glareproofed sapphire crystal and caseback, screw-locked crown. Mechanical excellence combined with quartz brings superior accuracy. A fixed inverted red triangle on the dial points to the chosen reference time. 429 brilliant-cut diamonds: approx 2.56 carats (case and bracelet). 112 brilliant-cut diamonds: approx 0.67 carats (dial). Diamond-set 18-carat pink gold dial, pink gold Royal Oak hands with luminescent coating.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Bvlgari',
            'price' => '3365.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77J354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'bvlgari_2.jpg',
            'short_description' => 'This exceptional selfwinding movement combines functionality and precision timekeeping with the highest standard of aesthetics.',
            'long_description' => 'Selfwinding watch with date display and centre seconds. Stainless steel case, black dial, stainless steel bracelet. Stainless steel case, glareproofed sapphire crystal and caseback, screw-locked crown. Black dial with white gold applied hour-markers and Royal Oak hands with luminescent coating. Powered by the natural movements of your wrist. Stainless steel bracelet with AP folding clasp.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Bvlgari',
            'price' => '6876.67',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77L354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'bvlgari_3.jpg',
            'short_description' => 'Powered by the natural movements of your wrist. Selfwinding chronograph with date display and small seconds at 6 oclock. Stainless steel case, silvered dial, stainless steel bracelet.',
            'long_description' => 'Silver-toned dial with Grande Tapisserie pattern, white gold applied hour-markers and Royal Oak hands with luminescent coating. Stainless steel bracelet with AP folding clasp. Protected by seven patents, it is one of the most complex calibres ever developed by the brand. The dials now feature rectangular index hour markers and longer hands, as well as a Chromalight display with long-lasting luminescence, which enhances legibility.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Chopard',
            'price' => '7333.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77N354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'chopard_men_1.jpg',
            'short_description' => 'Black dial with Grande Tapisserie pattern, silver-toned counters, white gold applied hour-markers and Royal Oak hands with luminescent coating',
            'long_description' => 'The 20th anniversary of Royal Oak chronographs marks a return to the two-tone dial design launched in 2008 – with subtle new details. This sleek stainless steel edition features a black “Grande Tapisserie” dial with bigger chronograph counters and new typeset, transfers, finishes and brighter luminescent coating for optimised readability.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Chopard',
            'price' => '7444.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77O354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'chopard_men_2.jpg',
            'short_description' => 'Blue dial with Grande Tapisserie pattern, white gold applied hourmarkers and Royal Oak hands with luminescent coating, blue inner bezel.',
            'long_description' => 'The new selfwinding calibre 5134 is based on its predecessor, calibre 2120, however it has been enlarged in accordance with the updated 41 mm case size. The highly finished 4.31 mm thick movement is fully visible through the glareproofed sapphire crystal caseback. Stainless steel bracelet with AP folding clasp.  It is equipped with a folding Oysterclasp and also features the ingenious Rolex-patented Easylink rapid extension system.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Chopard',
            'price' => '6645.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77Q354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'chopard_men_3.jpg',
            'short_description' => 'Highly legible Chromalight display with long-lasting blue luminescence. Folding Oysterclasp with Easylink 5 mm comfort extension link.',
            'long_description' => 'This model with its ultra-thin mechanical heart embodies a superbly interpreted encounter between the ancestral art of openworking and the three major types of horological complication: short-time measurement, striking mechanisms, and astronomical indications. Sapphire dial, black indicators, white gold applied hour-markers and hands. Hand-stitched large square scale black alligator strap with titanium AP folding clasp.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Chopard',
            'price' => '9677.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77S354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'chopard_men_4.jpg',
            'short_description' => 'From dial to hornback alligator strap, the crisp, slate-grey colourway of this timepiece lends a high-tech designer aesthetic, as perfect with a white t-shirt as a suit.',
            'long_description' => 'Slate grey dial with Méga Tapisserie, black counters, black Arabic numerals with luminescent coating, white gold Royal Oak hands with luminescent coating, black inner bezel ring. Hand-stitched Hornback grey alligator strap with stainless steel pin buckle. Stainless steel case, glareproofed sapphire crystal and caseback, black ceramic screw-locked crown and pushpieces.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Montblanc',
            'price' => '9475.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77T354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'montblanc_men_3.jpg',
            'short_description' => 'A perfect alchemy of form and function, aesthetics and technology, designed to be both robust and comfortable. Certified Swiss chronometer, a designation reserved for high-precision watches that have successfully passed the Swiss Official Chronometer Testing Institute (COSC) tests.',
            'long_description' => 'The sleek dark-blue Royal Oak Offshore Diver in the new Funky Colour collection, with easy to switch out rubber straps in blue or yellow. An exceptional diving watch, water-resistant to 300m, with bright yellow diving scale and zone from 60 to 15 minutes on the rotating inner bezel. Stainless steel case, glareproofed sapphire crystal and caseback, blue rubber-clad screw-locked crowns.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Montblanc',
            'price' => '8666.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77U354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'montblanc_men_5.jpg',
            'short_description' => 'Hammered 18-carat pink gold case, glareproofed sapphire crystal, water-resistant to 50 m. Silver-toned dial with “Grande Tapisserie” pattern, pink gold applied hour-markers and Royal Oak hands with luminescent coating.',
            'long_description' => 'The iconic Royal Oak for women celebrates its 40th anniversary with a showcase of ancient Florentine gold hammering jewellery technique. The 33 mm design features shimmering hammered 18-carat pink gold on its case, bezel and bracelet and a silvered Grande Tapisserie dial. Mechanical excellence combined with quartz brings superior accuracy.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Rolex',
            'price' => '8877.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77V354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'rolex_men_10.jpg',
            'short_description' => '18-carat pink gold case, glareproofed sapphire crystal, diamond-set bezel, screw-locked crown, water-resistant to 50 m. White rubber strap with 18-carat pink gold AP folding clasp. 32 brilliant-cut diamonds; approx 1.02 carats (bezel).',
            'long_description' => 'A new Lady Tapisserie design gives the dial of this Royal Oak Offshore chronograph a softer look, perfectly offsetting the 37 mm case size. A precise circular grain sweeps around the dial, playing with light and shadow. Silver-toned dial with circular-grained external zone, pink gold applied hour-markers and Royal Oak hands with luminescent coating.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Aerowatch',
            'price' => '2844.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77W354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'sports_watch_2.jpg',
            'short_description' => 'Black dial with Méga Tapisserie pattern, black counters, pink gold applied hour-markers and Royal Oak hands with luminescent coating, black inner bezel',
            'long_description' => 'Chronograph, hours, minutes, small seconds and date. Black ceramic case, glareproofed sapphire, titanium glareproofed sapphire crystal caseback, black ceramic bezel, screw-locked crown and pushpieces, titanium pushpiece guards and links. Black rubber strap with titanium pin buckle. Centre hour, minute and seconds hands. Stop-seconds for precise time setting.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Air Blue',
            'price' => '9684.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77X354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'sports_watch_5.jpg',
            'short_description' => 'Slate grey dial with Grande Tapisserie pattern, black counters, white gold applied hour-markers and Royal Oak hands with luminescent coating, black inner bezel. Black ceramic bracelet with titanium AP folding clasp.',
            'long_description' => 'Crafted in hand-finished black ceramic, this Royal Oak Perpetual Calendar features day, date, month, astronomical moon, and week of the year. The leap year indication — pioneered by Bvlgari in 1955 — is also featured on the Grande Tapisserie decorated dial. Black ceramic case, glareproofed sapphire crystal and caseback.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Alpina',
            'price' => '1243.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77Y354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'sports_watch_7.jpg',
            'short_description' => '18-carat yellow gold case, glareproofed sapphire crystal and caseback, 18-carat yellow gold bezel, black ceramic pushpieces and screw-locked crown. Black rubber strap with 18-carat yellow gold pin buckle.',
            'long_description' => 'A modern and technical design for a next-generation timepiece of optimised performance and aesthetics. The exclusive-production piece is in 18-carat yellow gold, intricately openworked to reveal the sophisticated hand-finished decoration and engineering within. Our hand-wound watches are celebrated for their reliability. Sapphire dial, black and white transfer counters, yellow gold Royal Oak hands with luminescent coating, black inner bezel.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Swiss Army',
            'price' => '5534.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77Z354GF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'swiss_army_3.jpg',
            'short_description' => 'Hand-wound tourbillon watch with GMT function and selection indicator. Titanium case, openworked dial, black strap. Titanium case, glareproofed sapphire crystal and caseback, black ceramic bezel, screw-locked crown and pushpieces.',
            'long_description' => 'Second time-zone indicator at 3 oclock, crown position indicator at 6 oclock, tourbillon at 9 oclock with blackened bridge, black ceramic central bridge, white gold Royal Oak hands, black anodized aluminium inner bezel. Crafted from ceramic, the central bridge of the calibre 2913 resonates with the white bezel, crown, and second time-zone corrector pushpiece. Ceramics are nine times harder than steel, so the craftsman’s work is particularly painstaking, complex and precise.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Tag Hauer',
            'price' => '7345.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354HF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'tag_men_1.jpg',
            'short_description' => 'The Equation of Time model with its bold central hand enables a precise reading of this daily variation, while the sunrise and sunset times in a location chosen by the owner are displayed on the 3 and 9 oclock counters.',
            'long_description' => 'Silver-toned dial with Grande Tapisserie pattern, white gold applied hour-markers and Royal Oak hands with luminescent coating. Hand-stitched large square scale alligator strap with stainless steel AP folding clasp. Highly legible Chromalight display with long-lasting blue luminescence. Perpetual, mechanical , self-winding , with a magnetic shield to protect the movement. Stainless steel case, glareproofed sapphire crystal and caseback, screw-locked crown.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Tag Hauer',
            'price' => '5533.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354IF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'tag_men_2.jpg',
            'short_description' => 'Selfwinding watch with date display and centre seconds. 18-carat pink gold case, silvered dial, brown strap. 18-carat pink gold case, glareproofed sapphire crystal and caseback, screw-locked crown.',
            'long_description' => 'Hand-stitched large scale brown alligator strap with 18-carat pink gold AP folding clasp. It is equipped with an Oysterclasp and the Easylink comfort extension link. This ingenious system allows the wearer to increase the bracelet length by approximately 5 mm, providing additional comfort in any circumstance.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Tag Hauer',
            'price' => '6644.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354JF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'tag_men_3.jpg',
            'short_description' => 'The slender, architectural form of the Royal Oak is outlined in a 33 mm case size and a two-tone material mix: steel and 18-carat pink gold. A pink gold-toned Grande Tapisserie dial beautifully underlines the design of this horological icon.',
            'long_description' => 'Stainless steel case, glareproofed sapphire crystal, 18-carat pink gold bezel, crown and links, water-resistant to 50 m. Stainless steel and 18-carat pink gold bracelet with stainless steel AP folding clasp. The Cellini collection is a contemporary celebration of classicism and the eternal elegance of traditional timepieces. To preserve the beauty of its pink gold watches, Crayo created and patented an exclusive 18 ct pink gold alloy cast in its own foundry: Everose gold. Introduced in 2005.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        /**
         * Female Category
         */
        DB::table('watches')->insert([
            'title' => 'Audemars',
            'price' => '5577.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354KF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'audemars_women_5.jpg',
            'short_description' => 'Quartz watch with date display. 18-carat yellow gold case, white mother-of-pearl dial, white strap. 32 brilliant-cut diamonds, approx 0.65 carats. Availability to be confirmed.',
            'long_description' => 'Glareproofed sapphire crystal, diamond-set bezel, crown set with a translucent cabochon sapphire, water-resistant to 50 m. Hand-stitched large square scale crocodile or 18-carat yellow gold with 18-carat yellow gold AP folding clasp. 32 brilliant-cut diamonds, approx 0.65 carats (bezel). Diamond or yellow gold applied hour-markers, yellow gold applied aperture.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Bvlgari',
            'price' => '8859.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354LF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'bvlgari_women_4.jpg',
            'short_description' => 'Hand-wound tourbillon chronograph with small seconds at 9 oclock. Stainless steel case, black dial, black strap.',
            'long_description' => 'Stainless steel case, glareproofed sapphire crystal and caseback, black rubber-clad bezel, screw-locked crown. Black dial with Grande Tapisserie pattern, white gold applied hour-markers and Royal Oak hands with luminescent coating. Black rubber strap with stainless steel AP folding clasp. The stylized, stretched applique hour markers are divided by a minute track that has been moved toward the center of the dial, closer to the tips of the hands.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Bvlgari',
            'price' => '7354.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354MF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'bvlgari_women_10.jpg',
            'short_description' => 'Silver ceramic case, glareproofed sapphire, titanium glareproofed sapphire crystal caseback, black ceramic bezel, screw-locked crown and pushpieces, titanium pushpiece guards and links.',
            'long_description' => 'Chronograph, hours, minutes, small seconds and date. Black rubber strap with titanium pin buckle. Black dial with Méga Tapisserie pattern, black counters, pink gold applied hour-markers and Royal Oak hands with luminescent coating, black inner bezel.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Michael Kors',
            'price' => '6745.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354NF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'michael_kors_women_1.jpg',
            'short_description' => 'Special series dedicated to the 25th anniversary of the Royal Oak Offshore. Black dial, white gold hands with luminescent coating. 904L is mainly used in the high technology, aerospace and chemical industries, where maximum resistance to corrosion is essential.',
            'long_description' => 'Stainless steel case, glareproofed sapphire crystal and caseback, stainless steel bezel, black ceramic screw-locked crown and pushpieces, sandblasted titanium pushpiece guards. Black rubber strap with stainless steel pin buckle. The Air-King dial features a distinctive black dial with a combination of large 3, 6 and 9 numerals marking the hours and a prominent minute scale for navigational time readings.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Michael Kors',
            'price' => '1243.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354OF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'michael_kors_women_4.jpg',
            'short_description' => 'A noble version of the haute horlogerie masterwork, containing 648 hand-finished parts, with full sporting functions and perpetual calendar, in a limited edition of three.',
            'long_description' => 'The black dial has the large hands and indices reserved for professional models. The innovative Chromalight display on the dial pushes back the boundaries of visibility in dark environments. Its distinctive blue glow lasts up to eight hours with a uniform luminosity, practically twice as long as that of standard luminescent materials.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Omega',
            'price' => '8456.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354PF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'omega_1.jpg',
            'short_description' => 'Sandblasted titanium case, glareproofed sapphire crystal and caseback, titanium pushpieces, pushpiece guards and screw-locked crown. Centre hour, minute and seconds hands.',
            'long_description' => 'In the purest traditional style, the Earth Dual Time is fitted on a remborded and stitched alligator leather strap with large scales. The 18 ct gold buckle matches the gold of the watch case. The sandblasted titanium case, crown and push piece give it a very sophisticated and contemporary look. Second time zone at 6 oclock with day/night indicator.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Rolex',
            'price' => '4576.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354QF38',
            'company_cost' => '4000',
            'instock' => '2',
            'shipping_cost' => '50',
            'image' => 'rolex_women_4.jpg',
            'short_description' => '18-carat white gold case, glareproofed sapphire crystal and caseback, crown set with a blue cabochon sapphire, water-resistant to 20 m. white gold off-centred disc, white mother-of-pearl small seconds counter with screw-set ring,',
            'long_description' => 'Hand-wound watch with small seconds. 18-carat white gold case, entirely set with diamonds, crown set with a blue cabochon sapphire. Diamond-paved white gold off-centred disc, white mother-of-pearl small seconds counter. Large square scale pearly dark grey alligator strap. Additional black alligator strap.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Chopard',
            'price' => '6645.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354RF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'chopard_men_7.jpg',
            'short_description' => 'Designed specifically for the watchs oval shape and masculine style, calibre 5201 is tailor made and hand-wound. Operating at a frequency of 3 Hertz, it contains 157 parts and 19 jewels.',
            'long_description' => 'Hand-wound watch with small seconds. 18-carat white gold case, entirely set with diamonds, crown set with a blue cabochon sapphire. Diamond-paved black gold off-centred disc, white mother-of-pearl small seconds counter. Large square scale pearly dark grey alligator strap. Glareproofed sapphire crystal and caseback, water-resistant to 20 m.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Chopard',
            'price' => '6645.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354SF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'chopard_men_8.jpg',
            'short_description' => 'Bright crimson dial with “Méga Tapisserie” decoration, white gold applied hour-markers and Royal Oak hands. A rugged timepiece with serious diving attributes and a very fresh and fun appeal.',
            'long_description' => 'Crimson dial with “Méga Tapisserie” pattern, white gold applied hour-markers and Royal Oak hands with luminescent coating, blue rotating inner bezel with diving scale and zone from 60 to 15 minutes in crimson. Black rubber strap with stainless steel pin buckle. Additional blue rubber strap.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Chopard',
            'price' => '7745.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354TF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'chopard_men_9.jpg',
            'short_description' => 'Stainless steel case, glareproofed sapphire crystal and caseback, blue rubber-clad screw-locked crowns. Oscillating weight with ceramic ball bearings. Inverted snailing on bridges.',
            'long_description' => 'The sleek black Royal Oak Offshore Diver in the new Deep Water collection, with easy to switch out rubber straps in black or blue. An exceptional diving watch, water-resistant to 300m, with bright white diving scale and zone from 60 to 15 minutes on the rotating inner bezel. White gold applied hour-markers and Royal Oak hands with luminescent coating.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        /**
         * Female Category
         */






        DB::table('watches')->insert([
            'title' => 'Chopard',
            'price' => '9605.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354TF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'chopard_men_10.jpg',
            'short_description' => 'Black ceramic case, glareproofed sapphire crystal and caseback. Slate grey dial, black counters, white gold applied hour-markers and Royal Oak hands with luminescent coating, black inner bezel.',
            'long_description' => 'Crafted in hand-finished black ceramic, this Royal Oak Perpetual Calendar features day, date, month, astronomical moon, and week of the year. The leap year indication — pioneered by Audemars Piguet in 1955 — is also featured on the Grande Tapisserie decorated dial. Black rubber bracelet with titanium AP folding clasp. Six-hand chronograph movement features proprietary high-performance quartz technology with 262 kHz vibrational frequency for precise accuracy.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Glycine',
            'price' => '3356.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354UF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'maurice_women_1.jpg',
            'short_description' => 'From the Ladies Diamonds Collection. Stainless steel case with 23 diamonds individually hand set on rose gold-tone accented bezel and dial, white mother-of-pearl inner dial, sapphire glass, screw-back case, and water resistance to 100 meters.',
            'long_description' => 'Hand-wound watch with tourbillon. 18-carat white gold case, entirely set with diamonds. 18-carat white gold dial. Snow-set flower, pink gold hands. Large square scale pearly white alligator strap. 424 brilliant-cut diamonds ; ~3.95 carats (case, buckle). 504 brilliant-cut diamonds ; ~1.12 carats (flower, rings). 213 brilliant-cut diamonds ; ~0.25 carats (movement).',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Giorgio Fedon 1919',
            'price' => '7745.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354VF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'maurice_women_2.jpg',
            'short_description' => 'Six-hand chronograph movement features proprietary high-performance quartz technology with 262 kHz vibrational frequency for precise accuracy. Double-domed mineral glass, smooth grain wwhite leather strap with three-piece buckle closure. ',
            'long_description' => 'From the Maurice Diamonds Collection. In stainless steel with rose-gold ion-plated finish, 12 diamonds individually hand-set on a white mother-of-pearl dial with rose-gold accents, domed sapphire crystal, white enamel bezel accents, luminous hands, second hand, and white leather strap. Six-hand chronograph function in stainless steel case.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Henry Dunay',
            'price' => '6634.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354WF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'maurice_women_3.jpg',
            'short_description' => 'Large square scale pearly white alligator strap with diamond-set 18-carat white gold AP folding clasp. Additional black alligator strap. Our hand-wound watches are celebrated for their reliability.',
            'long_description' => 'Hand-wound watch with tourbillon. 18-carat white gold case, entirely set with diamonds. 18-carat white gold dial. Snow-set flower, pink gold hands. 424 brilliant-cut diamonds; approx 3.95 carats (case, buckle). 504 brilliant-cut diamonds ;  approx 1.12 carats (flower, rings). 213 brilliant-cut diamonds ;approx 0.25 carats (movement).',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Guy Laroche',
            'price' => '7456.00',
            'category_id' => '1',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354XF38',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'maurice_women_4.jpg',
            'short_description' => 'The Oyster bracelet is a perfect alchemy of form and function, aesthetics and technology, designed to be both robust and comfortable. It is equipped with an Oysterclasp and the Easylink comfort extension link.',
            'long_description' => 'Stainless steel case with a Silver metal bracelet. Black dial with silver-tone hands and dot hour markers. Case diameter (without crown): 22 mm. Case diameter with crown: 24 mm. Solid case back. Case thickness: 7 mm. Unique heart case shape. Japanese quartz movement. Jewelry clasp. Water resistant up to 10 meters (33 feet).',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);        

        DB::table('watches')->insert([
            'title' => 'Jet Set',
            'price' => '6456.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354GF48',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'sports_watch_5.jpg',
            'short_description' => 'Hand-stitched large scale black alligator strap with 18-carat pink gold AP folding claspPowered by the natural movements of your wrist. This exceptional selfwinding movement combines functionality and precision timekeeping with the highest standard of aesthetics.',
            'long_description' => ' Characterised by hour markers fashioned from 18 ct gold to prevent tarnishing, the dial is designed and manufactured in-house, largely by hand to ensure perfection. Tested a second time after being cased to ensure that in everyday use it satisfies criteria for precision, which is twice as exacting as those for an officially certified chronometer. This control of the final precision of the assembled watch is carried out using a methodology and high-technology equipment specially developed.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        

        DB::table('watches')->insert([
            'title' => 'Luminox',
            'price' => '7745.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '77A354GF68',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'men_3.jpg',
            'short_description' => 'To celebrate the 20th anniversary of its game-changing launch in 1997, the all-titanium Royal Oak Chronograph, one of the true icons of modern haute horlogerie, gets a two-tone Grande Tapisserie dial design.',
            'long_description' => 'The Cosmograph Daytona is equipped with calibre 4130, a self-winding mechanical chronograph movement. Its architecture incorporates far fewer components than a standard chronograph, thereby enhancing the movement’s reliability. Slate grey dial with Grande Tapisserie pattern, red counters, white gold applied hour-markers and Royal Oak hands with luminescent coating. Chronograph, hours, minutes, small seconds and date functions.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Jill Stuart',
            'price' => '6755.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'men_4.jppg',
            'short_description' => 'The chronograph movement features a Parachrom hairspring, offering greater resistance to shocks and to temperature variations. Functions: day, date, hour, minute, seconds. Casual watch style.Jill Stuart Retrograde Watch Silde006.',
            'long_description' => 'Stainless steel case with a black synthetic bracelet. Fixed stainless steel bezel. Charcoal dial with silver-tone hands and Swarovski crystal hour markers. Dial Type: Analog. Three sub-dials displaying: seconds, date and day of the week. Japanese quartz movement. Scratch resistant mineral crystal. Solid case back. Case diameter: 35 mm. Case thickness: 9 mm. Round case shape. Band width: 18 mm. Tang clasp. Water resistant at 30 meters (100 feet).',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Juicy Couture',
            'price' => '8356.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'men_5.jpg',
            'short_description' => 'Centre hour, minute and seconds hands, small seconds hand at 6 oclock. Chronograph (centre hand) accurate to within 1/8 of a second, 30-minute counter at 3 oclock and 12-hour counter at 9 oclock. Stop seconds for precise time setting.',
            'long_description' => 'Stainless steel case with a black leather strap. Fixed stainless steel bezel. Black dial with silver-tone hands and pink crystal hour markers. Dial Type: Analog. Three sub-dials displaying: seconds, date and day of the week. Japanese quartz movement. Scratch resistant mineral crystal. Solid case back. Case diameter: 35 mm. Case thickness: 9 mm. Round case shape. Band width: 18 mm. Tang clasp. Water resistant at 10 meters (33 feet). Functions: day, date, hour, minute, seconds. Casual watch style.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'MOMO Design',
            'price' => '3564.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'men_6.jpg',
            'short_description' => 'Combining the 3 categories of horological complications represented by short-time measurement, striking mechanisms and astronomical indications, this model driven by in-house calibre 2885 was hand-crafted by a single master-watchmaker in the grande complication workshop.',
            'long_description' => 'Perpetual calendar with moon phase indicates the day, date, month, week, year including leap year and moon phase. Monobloc middle case, screw-down case back and winding crown. Stainless steel case with a Black leather bracelet. Black dial with a analog display. Case diameter without crown: 19 mm. Case diameter with crown: 21 mm. Case thickness: 6 mm. Japanese quartz movement. Tang clasp. Water resistant up to 10 meters (33 feet).',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Milus',
            'price' => '4756.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'men_7.jpg',
            'short_description' => 'Titanium case, glareproofed sapphire crystal, titanium bezel, black ceramic screw-locked crown, black ceramic and titanium pushpieces, titanium pushpiece guards, water-resistant to 20 m. Black rubber strap with titanium AP folding clasp.',
            'long_description' => 'Presented in a super-light titanium case, this exclusive-production timepiece is precision-made to awaken sound. It brings unprecedented acoustic performance, exceptionally refined sound quality and the clear, harmonic tone of traditional pocket-watches to a contemporary minute repeater wristwatch. Black dial, openworked, satin-brushed, black counters, white gold hands with luminescent coating, black inner bezel.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Mido',
            'price' => '7798.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'men_8.jpg',
            'short_description' => 'A new Men Tapisserie design gives the dial of this Royal Oak Offshore chronograph a softer look, perfectly offsetting the 37 mm case size. A precise circular grain sweeps around the dial, playing with light and shadow.',
            'long_description' => 'Stainless steel case, glareproofed sapphire crystal, diamond-set bezel, screw-locked crown, water-resistant to 50 m. Black dial with new Tapisserie pattern, circular-grained external zone, white gold applied hour-markers and Royal Oak hands with luminescent coating. Black rubber strap with stainless steel AP folding clasp.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Oris',
            'price' => '3568.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'men_9.jpg',
            'short_description' => 'In titanium, this high-accuracy tourbillon showcases Oris expertise in hand-finished decoration and ingenious engineering techniques, hand-sculpted curves, satin brushed surfaces and polished beveled edges. Titanium case, glareproofed sapphire crystal and caseback.',
            'long_description' => 'Ruthenium-toned dial, openworked, satin-brushed, ruthenium-toned counters, pink gold applied hour-markers and Royal Oak hands with luminescent coating, ruthenium-toned inner bezel. Titanium bracelet with AP folding clasp. Additional grey alligator strap. Centre hour, minute and seconds hands. Instantaneous date with rapid setting. Stop-seconds for precise time setting.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Obaku',
            'price' => '9475.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'men_10.jpg',
            'short_description' => 'The Bell Ross collection is a contemporary celebration of classicism and the eternal elegance of traditional timepieces, combining know-how and high standards of perfection with an approach that heightens watchmaking heritage in its most timeless form.',
            'long_description' => 'Dial Type: Analog. Japanese quartz movement. Scratch resistant mineral crystal. Swarovski crystal-set crown. Solid case back. Case diameter: 30 mm. Case thickness: 12 mm. Round case shape. Band width: 22 mm. Tang clasp. Water resistant at 30 meters / 100 feet. Functions: hour, minute, second.  Fixed stainless steel bezel. White dial with silver-tone hands and Swarovski crystal markers at the quarter hour positions.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'Morphic',
            'price' => '3465.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'men_selection_2.jpg',
            'short_description' => 'The Rolex Pearlmaster is Rolexs crowning jewellery watch. Featuring the softly curved lines of the Pearlmaster design, it is characterissed by uniquely rich dials and exquisite gem settings of diamonds, sapphires or rubies and available only in 18 ct yellow, white or Everose gold made by rolex in its own foundry.',
            'long_description' => 'To preserve the beauty of its pink gold watches, Rolex created and patented an exclusive 18 ct pink gold alloy cast in its own foundry: Everose gold. Introduced in 2005, 18 ct Everose is used on all Rolex models in pink gold. The design, development and production of Rolex bracelets and clasps, as well as the stringent tests they face, involve advanced high technology. And, as with all the components of the watch, aesthetic controls by the human eye guarantee impeccable beauty.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('watches')->insert([
            'title' => 'MVMT',
            'price' => '5633.00',
            'category_id' => '2',
            'weight' => '100g',
            'length' => '200mm',
            'size' => '22',
            'sku' => '',
            'company_cost' => '4000',
            'instock' => '1',
            'shipping_cost' => '50',
            'image' => 'men_selection_5.jpg',
            'short_description' => 'Stainless steel case, glareproofed sapphire crystal. White gold applied hour-markers and Royal Oak hands with luminescent coating. The oscillating weight is guided by a peripheral ring rolling on four ruby runners, which reduces friction and wear to the minimum possible.',
            'long_description' => 'The new Oyster 34 is the first model equipped with calibre 2236 and the new Syloxi hairspring. This self-winding mechanical movement with date display is entirely manufactured by MVMT and inaugurates a new generation of movements. Its design and quality manufacture ensure peerless precision and reliability. Second timezone, day and night indicator, power reserve, hours, minutes and date.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);        
     
    }
}
