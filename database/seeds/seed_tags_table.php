<?php

use Illuminate\Database\Seeder;
use \Carbon\Carbon;

class seed_tags_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
        	'id' => '1',
        	'tag_name' => 'Trending',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '2',
        	'tag_name' => 'Sports',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '3',
        	'tag_name' => 'Expandable',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '4',
        	'tag_name' => 'Alarm',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '5',
        	'tag_name' => 'Mechanical',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);   
        
        DB::table('tags')->insert([
        	'id' => '6',
        	'tag_name' => 'Oversized',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '7',
        	'tag_name' => 'Digital',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '8',
        	'tag_name' => 'Designer',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '9',
        	'tag_name' => 'Slim',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '10',
        	'tag_name' => 'Waterproof',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);    

		DB::table('tags')->insert([
        	'id' => '11',
        	'tag_name' => 'Analogue',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '12',
        	'tag_name' => 'Compass',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '13',
        	'tag_name' => 'Solar Powered',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '14',
        	'tag_name' => 'Luxury',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '15',
        	'tag_name' => 'Ceramic',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);   
        
        DB::table('tags')->insert([
        	'id' => '16',
        	'tag_name' => 'Leather Band',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '17',
        	'tag_name' => 'Diver\'s',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '18',
        	'tag_name' => 'Pocket Watch',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '19',
        	'tag_name' => 'Chronograph',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tags')->insert([
        	'id' => '20',
        	'tag_name' => 'Clearance',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
