<?php

use Illuminate\Database\Seeder;

class seed_users_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'is_admin' => 1,
        'name' => 'nicole peters',
        'email' => 'nicole@hotmail.com',
        'username' => 'npeters',
        'street' => '100 Pleasant St',
        'postal' => 'R2R A6G',
        'city' => 'Calgary',
        'province' => 'Alberta',
        'country' => 'Canada',
        'password' => bcrypt('secret'),
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
      ]);

      DB::table('users')->insert([
        'is_admin' => 0,
        'name' => 'anushka panagar ',
        'email' => 'anushkapanagar@gmail.com',
        'username' => 'anushka',
        'street' => '1135 Byng Place',
        'postal' => 'R2R G6F',
        'city' => 'Calgary',
        'province' => 'Alberta',
        'country' => 'Canada',
        'password' => bcrypt('secret'),
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
      ]);
    }
}
