<?php

use Illuminate\Database\Seeder;

class seed_tag_watch_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tag_watch')->insert([
            'watch_id' => 1,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 2,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 3,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 4,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 5,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 6,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 7,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 8,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 9,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 10,
            'tag_id' => 1,
        ]);


        DB::table('tag_watch')->insert([
            'watch_id' => 11,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 12,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 13,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 14,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 15,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 16,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 17,
            'tag_id' => 1,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 18,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 19,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 20,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 21,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 22,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 23,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 24,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 25,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 26,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 27,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 28,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 29,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 30,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 31,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 32,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 33,
            'tag_id' => 2,
        ]);

        DB::table('tag_watch')->insert([
            'watch_id' => 34,
            'tag_id' => 2,
        ]);


    }
}
