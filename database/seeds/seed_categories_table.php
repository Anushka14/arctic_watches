<?php

use Illuminate\Database\Seeder;

class seed_categories_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'id'=>'1',
            'category_name'=>'Hers'
        ]);

        DB::table('categories')->insert([
            'id'=>'2',
            'category_name'=>'His'
        ]);

    }
}
