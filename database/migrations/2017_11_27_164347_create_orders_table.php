<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('email');
            $table->string('username');
            $table->string('street');
            $table->string('postal');
            $table->string('city');
            $table->string('province');
            $table->string('country');
            $table->string('shipping_street');
            $table->string('shipping_postal');
            $table->string('shipping_city');
            $table->string('shipping_province');
            $table->string('shipping_country');
            $table->decimal('subtotal', 12, 2);
            $table->decimal('taxes', 12, 2);
            $table->decimal('total_price', 12, 2);
         
            $table->string('transaction_status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
