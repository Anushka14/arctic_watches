<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('watches', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title');
            $table->float('price')->format(7,2);
            $table->integer('category_id');
            $table->string('weight');
            $table->string('length');
            $table->integer('size');
            $table->string('sku');
            $table->float('company_cost')->format(7,2);
            $table->boolean('instock');
            $table->float('shipping_cost')->format(7,2);
            $table->string('image');
            $table->text('short_description');
            $table->text('long_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('watches');
    }
}
